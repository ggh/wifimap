//
//  WMViewController.m
//  WifiMap
//
//  Created by Gor on 22/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import "WMMainViewController.h"
#import "WMPointsHandler.h"
#import "WMDBLayer.h"
#import MAP_H
#import "WMPlacesViewController.h"
#import "WMInfoWindow.h"
#import "WMPointDetailsViewController.h"
#import "WMNearbyViewController.h"
#import "WMNewPointViewController.h"

#if LITE_VERSION
#import "ClusterRegion.h"
typedef GMSMarker* MarkerType;
#else
//#import "WMDataHandler.h"
typedef RMAnnotation* MarkerType;
#endif


//#define SWITCH_ZOOM_LEVEL               13.5f
#define SWITCH_ZOOM_LEVEL               9.5f
#define POINT_CLUSTERIZATION_LEVEL      15.8f
#define CLUSTER_CLUSTERIZATION_LEVEL    7.5f

#define MAX_DISTANCE                    1500

#define kUSER_HOME_LOCATION             @"start_location"
//#define DISTANCE_FROM_HOME              1000000
#define DISTANCE_FROM_HOME              200

@interface WMMainViewController() <WMPointDetailsDelegate, UISearchBarDelegate, UISearchDisplayDelegate, CLLocationManagerDelegate,
UITableViewDataSource, UITableViewDelegate,
#if !LITE_VERSION
RMTileCacheBackgroundDelegate, RMMapViewDelegate>
#else
GMSMapViewDelegate>
#endif

{
}

@property (nonatomic, strong) MAP_VIEW *mapView;
@property BOOL isMapInited;
@property (nonatomic, strong) UIActivityIndicatorView *loadIndicator;
@property (nonatomic, strong) WMPointDetailsViewController *pointDetailsController;
@property (nonatomic, strong) UIButton *nearbyButton;

@property (nonatomic, strong) NSArray *allClusters;
@property (nonatomic, strong) NSArray *visiblePoints;
@property (nonatomic, strong) NSMutableArray *clusterMarkers;
@property (nonatomic, strong) NSMutableArray *pointMarkers;

@property (nonatomic, strong) CLLocationManager *locationManager;

// Places..
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIButton *locationButton;
- (IBAction)locationButtonAction;
@property (strong, nonatomic, readonly) UITextField *textField;
@property (strong, nonatomic, readonly) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic, readonly) UIView *searchIcon;

@property (strong, nonatomic) NSArray *searchResluts;

// TEST
@property BOOL shouldBlockRotation;

/**
 Pointer to searchDisplayController
 
 For iPhones it's the self searchDisplayController, for
 iPad - comtroller of child view controller
 */
@property (weak, nonatomic) UISearchDisplayController *searchController;


@property (strong, nonatomic) IBOutlet UIButton *saveButton;
// Download View
@property (strong, nonatomic) IBOutlet UIView *downloadView;
@property (strong, nonatomic) IBOutlet UIView *downloadTopView;
@property (strong, nonatomic) IBOutlet UILabel *downloadInfoLabel;
@property (strong, nonatomic) IBOutlet UIView *downloadBottomView;
@property (strong, nonatomic) IBOutlet UIButton *downloadOkButton;
@property (strong, nonatomic) IBOutlet UIButton *downloadCancelButton;
@property (strong, nonatomic) IBOutlet UIImageView *downloadMapImage;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *downloadViewTapGesture;
@property (strong, nonatomic) IBOutlet UIScrollView *downloadScrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *downloadPageControl;
@property (strong, nonatomic) IBOutlet UILabel *downloadProgressLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *downloadProgressBar;
@property BOOL isDownloading;
- (IBAction)closeDownloadView;

// WiFi Info View
@property (strong, nonatomic) IBOutlet UIView *wifiInfoView;
@property (strong, nonatomic) IBOutlet UILabel *wifiInfoLabel;
@property (strong, nonatomic) IBOutlet UIButton *addWifiButton;
@property (strong, nonatomic) NSString *currentWifiName;

@property (strong, nonatomic) WMNewPointViewController *newPointController;



// TEST
@property (strong, nonatomic) UIPopoverController *popController;



@property BOOL isZoomChanged;

/**
 Defines whether the map should switch from points mode to clusters
 or vice versa
 */
@property BOOL needsToSwitch;

/**
 Screen rectangles projected to map which represent the
 areas grid-based clustering

 Screen grid is an array of rectangles that cover entire screen.
 With this grid the screen is devided into 4x8 matrix. Each cell
 represents a cluster area. All the annotations within this regions
 (is it point or predefined cluster) will be grouped into one.
 */
@property (nonatomic, readonly, strong) NSMutableArray *clusteringRegions;

@property (nonatomic) BOOL isPointMode;

@property (nonatomic, strong) UIImage *mainPinImage;
@property (nonatomic, strong) UIImage *yellowPinImage;
@property (nonatomic, strong) UIImage *clusterPinImage;

@property (strong, nonatomic) IBOutlet UIView *testView;

@property (weak, nonatomic) UIView *dimmView;

@end



@implementation WMMainViewController

@synthesize allClusters = _allClusters;
@synthesize visiblePoints = _visiblePoints;
@synthesize clusterMarkers = _clusterMarkers;
@synthesize currentWifiName = _currentWifiName;
@synthesize newPointController = _newPointController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor greenColor];
    if(!IS_IPAD) {
        self.navigationController.navigationBarHidden = YES;
    }
    self.title = NSLocalizedString(@"map", @"");

    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];

    [self initMapView];
#if LITE_VERSION
    [self initClusteringRegions];
#endif
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchClustersFromDB)
                                                 name:nDID_UPDATE_CLUSTERS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchPointsFromDB:)
                                                 name:nDID_UPDATE_POINTS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wifiStateChanged)
                                                 name:kWIFI_STATE_UPDATED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideWifiView)
                                                 name:@"new_point_added" object:nil];


    [self initWifiInfoView];
    [self initSearchView];

    // make sure shared pointDetailsController always has delegate
    self.pointDetailsController.delegate = self;

    WMNearbyViewController *nearby = [WMNearbyViewController sharedController];
    NSNumber *maxDistance = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"MaxDistance"];
    NSNumber *maxVisibleDistance = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"MaxVisibleDistance"];
    NSNumber *maxVisibleDistanceExtended = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"MaxVisibleDistanceExtended"];
    nearby.maxDistance = maxDistance.doubleValue;
    nearby.maxVisibleDistance = [GUtils sharedUtils].didRateGood ? maxVisibleDistanceExtended.doubleValue : maxVisibleDistance.doubleValue;

    [GUtils enableReachabilityCheck:YES wifiOnly:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification object:nil];

    [self initDownloadView];
    [self.view bringSubviewToFront:self.saveButton];
}

- (void)reachabilityChanged:(NSNotification*)notification
{
//    Reachability *r = (Reachability*)notification.object;
//    BOOL reachable = r.isReachable;
#if !LITE_VERSION
//    if(!reachable) {
////        NSURL *url = [NSURL fileURLWithPath:[self savedDBPath]];
//        NSString *filePath = [self savedDBPath];
//        NSURL *url = [NSURL fileURLWithPath:filePath isDirectory:NO];
//        RMMBTilesSource *tileSource = [[RMMBTilesSource alloc] initWithTileSetURL:url];
//        [self.mapView setTileSource:tileSource];
//    }
#endif
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(IS_IOS7) {
//        self.navigationController.navigationBarHidden = YES;
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    else {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if(IS_IPAD) {
//        if(![self.childViewControllers.firstObject searchDisplayController].isActive) {
        if(!self.searchController.isActive) {
            [self setSearchViewActive:YES];
        }
        CGRect newFrame = [self.testView convertRect:self.nearbyButton.frame fromView:self.view];
        self.nearbyButton.frame = newFrame;
        [self.testView addSubview:self.nearbyButton];
        [self.testView sendSubviewToBack:self.nearbyButton];

    }
    if(!self.downloadView.isHidden) {
        [self.view bringSubviewToFront:self.downloadView];
    }

#if LITE_VERSION
    CGFloat rotAngle = M_PI_2;
    switch (toInterfaceOrientation) {
        case UIInterfaceOrientationLandscapeLeft:
            rotAngle = M_PI_2;
            break;
        case UIInterfaceOrientationLandscapeRight:
            rotAngle = -1 * M_PI_2;
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            rotAngle = 0;
            break;
        default:
            rotAngle = 0;
            break;
    }
    
    self.downloadView.transform = CGAffineTransformMakeRotation(rotAngle);
    CGRect frame = self.downloadView.bounds;
    frame.size = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height);
    self.downloadView.frame = frame;
#endif
    
//    [self rearrangeScrollViewForOrientation:toInterfaceOrientation];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if(!IS_IOS7) {
        [self.searchBar setBackgroundImage:[UIImage imageNamed:@"transparent.png"]];
        self.searchBar.backgroundColor = [UIColor clearColor];
    }

    if(IS_IPAD) {
        CGRect newFrame = [self.view convertRect:self.nearbyButton.frame fromView:self.testView];
        self.nearbyButton.frame = newFrame;
        [self.view addSubview:self.nearbyButton];

//        if(![self.childViewControllers.firstObject searchDisplayController].isActive) {
        if(!self.searchController.isActive) {
            [self setSearchViewActive:NO];
        }
        [self.view bringSubviewToFront:self.testView];
    }

    if(!self.downloadView.isHidden) {
        [self.view bringSubviewToFront:self.downloadView];
    }
    
#if LITE_VERSION
    [self refreshClusteringRegionAreas];
#endif
}

- (void)showDownloadForCurrentLocation
{
    [self goToUserLocation];
    [self performSelector:@selector(showDownloadViewAnimated) withObject:nil afterDelay:0.2f];
}

- (void)showDownloadViewAnimated
{
    [self showDownloadView:YES];
}

- (WMPointDetailsViewController*)pointDetailsController
{
    if(!_pointDetailsController) {
//        _pointDetailsController = [[WMPointDetailsViewController alloc] init];
//        [_pointDetailsController view];
        _pointDetailsController = [WMPointDetailsViewController sharedController];
        _pointDetailsController.delegate = self;
    }
    return _pointDetailsController;
}

- (UIImage*)mainPinImage
{
    if(!_mainPinImage) {
        _mainPinImage = [UIImage imageNamed:@"default_pin.png"];
    }
    return _mainPinImage;
}

- (UIImage*)yellowPinImage
{
    if(!_mainPinImage) {
        _mainPinImage = [UIImage imageNamed:@"yellow_pin.png"];
    }
    return _mainPinImage;
}

- (UIImage*)clusterPinImage
{
    if(!_clusterPinImage) {
        _clusterPinImage = [UIImage imageNamed:@"cluster_pin.png"];
    }
    return _clusterPinImage;
}

- (void)switchToPointsMode
{
    [self switchToPointsMode:1];
}

- (void)switchToPointsMode:(NSUInteger)areaSize
{
    [self hideClusters];
    CGRect area = self.mapView.bounds;
    CGFloat dx = (area.size.width - areaSize * area.size.width) / 2.f;
    CGFloat dy = (area.size.height - areaSize * area.size.height) / 2.f;
    area.size.height *= areaSize;
    area.size.width *= areaSize;
    area = CGRectOffset(area, dx, dy);
    DebugLog(@"dx: %g\tdy: %g", dx, dy);
#if LITE_VERSION
    GMSProjection *projection = self.mapView.projection;
    CLLocationCoordinate2D topLeft = [projection coordinateForPoint:CGPointMake(area.origin.x, area.origin.y)];
    CLLocationCoordinate2D bottomRight = [projection coordinateForPoint:CGPointMake(CGRectGetMaxX(area),
                                                                                    CGRectGetMaxY(area))];
#else
    CLLocationCoordinate2D topLeft = [self.mapView pixelToCoordinate:CGPointMake(area.origin.x, area.origin.y)];
    CLLocationCoordinate2D bottomRight = [self.mapView pixelToCoordinate:CGPointMake(CGRectGetMaxX(area),
                                                                                     CGRectGetMaxY(area))];
#endif

//    [WMPointsHandler getPointsForArea:topLeft bottomRight:bottomRight];
    self.isPointMode = YES;
    self.visiblePoints = [WMDBLayer pointsForArea:topLeft bottomRight:bottomRight];
    [self refreshPointMarkers];

#if LITE_VERSION
    [self clusterizeCurrentProjection:self.mapView.projection];
#endif

}

- (void)switchToClustersMode
{
//    return;
    DebugLog(@"switch to CLUSTER mode");
    if(!self.clusterMarkers || self.clusterMarkers.count == 0) {
        [self fetchClustersFromDB];
    }
    self.isPointMode = NO;
    [self hidePoints];
#if LITE_VERSION
    [self clusterizeCurrentProjection:self.mapView.projection];
#else
    [self refreshClusterMarkers];
#endif
}

- (void)requestFirstPoints
{
    [self hideClusters];
    CGPoint min = CGPointMake(-5 * self.view.bounds.size.width, -3.5f * self.view.bounds.size.height);
    CGPoint max = CGPointMake(5 * self.view.bounds.size.width, 3.5f * self.view.bounds.size.height);
#if LITE_VERSION
    GMSProjection *projection = self.mapView.projection;
    CLLocationCoordinate2D topLeft = [projection coordinateForPoint:min];
    CLLocationCoordinate2D bottomRight = [projection coordinateForPoint:max];
#else
    CLLocationCoordinate2D topLeft = [self.mapView pixelToCoordinate:min];
    CLLocationCoordinate2D bottomRight = [self.mapView pixelToCoordinate:max];
#endif

    self.isPointMode = YES;
    self.visiblePoints = [WMDBLayer pointsForArea:topLeft bottomRight:bottomRight];
#if LITE_VERSION
    [self clusterizeCurrentProjection:self.mapView.projection];
#endif
    [self refreshPointMarkers];
}

#pragma mark - MapView Delegates

#if LITE_VERSION

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    // Check if zooming..
    static float recentZoomLevel = 21;  // max zooom
    float currentZoomLevel = mapView.camera.zoom;
    if(currentZoomLevel == recentZoomLevel) {
        // zoom is changed. must update clusterized objects
        self.isZoomChanged = NO;
    }
    else {
        self.isZoomChanged = YES;
    }
    recentZoomLevel = currentZoomLevel;
//    DebugLog(@"current zoom: %g", currentZoomLevel);

    if(self.isZoomChanged && currentZoomLevel > SWITCH_ZOOM_LEVEL && !self.isPointMode) {
        self.needsToSwitch = YES;
//        [self switchToPointsMode];
    }
    else if(self.isZoomChanged && currentZoomLevel <= SWITCH_ZOOM_LEVEL && self.isPointMode) {
//        [self switchToClustersMode];
        self.needsToSwitch = YES;
    }
    else {
        self.needsToSwitch = NO;
    }

    // make sure updates are made twice a second at the most
    static NSTimeInterval recentUpdateTime = 0;
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval deltaTime = currentTime - recentUpdateTime;
    if(deltaTime <= 0.2f) {
        return;
    }
    recentUpdateTime = currentTime;

    if(self.isPointMode) {
        [self updateClusteringForCurrentView];
    }
    else {
        // if zoom < 7.5 don't update clusters here!
        // Update only after animation (at idle)
        if(currentZoomLevel >= 7.5) {
            [self updateClusteringForCurrentView];
        }
        else {
            if(!self.isZoomChanged) {
                // this may be slow on iPhone4S-
                [self updateClusteringForCurrentView];
            }
        }
    }

    [self updateUserLocationDistance];
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    if(self.isPointMode) {
        if(self.needsToSwitch) {
            [self switchToClustersMode];
        }
        else {
            [self updatePointsForCurrentView];
        }
    }
    else {
        if(self.needsToSwitch) {
            [self switchToPointsMode];
        }
        else {
            [self updateClusteringForCurrentView];
        }
    }
}

- (UIView*)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
//    return nil;
    if(!self.isPointMode) {
        return nil;
    }

    marker.infoWindowAnchor = CGPointMake(0.5f, 0.3);
    NSString *identifier = marker.userData;
    WMPoint *point;// = [WMDBLayer pointWithId:identifier];
    BOOL disabledPoint = YES;

    for(WMPoint *p in self.visiblePoints) {
        if([p.identifier isEqualToString:identifier]) {
            point = p;
            break;
        }
    }

#if LITE_VERSION
    CLLocationDistance distance = [mapView.myLocation distanceFromLocation:[[CLLocation alloc]
                                                                            initWithLatitude:point.lat.doubleValue
                                                                            longitude:point.lon.doubleValue]];
    if(distance <= [WMNearbyViewController sharedController].maxVisibleDistance) {
        disabledPoint = NO;
    }
#endif

    UIView *view = [WMInfoWindow infoWindowForPoint:point disabled:disabledPoint];

    return view;
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    if(mapView.selectedMarker != nil) {
        mapView.selectedMarker = nil;
    }
    else {
        [self switchSearchBarVisibility];
    }
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    NSString *identifier = marker.userData;
    WMPoint *point = [WMDBLayer pointWithId:identifier];

    // assuming that google maps can appear in pro version later..
#if LITE_VERSION
    CLLocationDistance distance = [mapView.myLocation distanceFromLocation:[[CLLocation alloc]
                                                                            initWithLatitude:point.lat.doubleValue
                                                                            longitude:point.lon.doubleValue]];
    if(distance > [WMNearbyViewController sharedController].maxVisibleDistance) {
        return;
    }
#endif

    self.pointDetailsController.point = point;
    if(IS_IPAD) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"showPointDetails" object:nil userInfo:nil];
    }
    else {
        [self.navigationController pushViewController:self.pointDetailsController animated:YES];
    }
}

- (void)testMoveView:(UIView*)view
{
    DebugLog(@"testmove..");
    if(view == nil) {
        DebugLog(@"view is nil");
    }
    else {
        DebugLog(@"view type: %@", [[view class] description]);
        for(UIView *sub in view.subviews) {
            DebugLog(@"subView: %@\nuserinteractions: %d", [[sub class] description], sub.userInteractionEnabled);
        }
        DebugLog(@"user interacions: %d", view.userInteractionEnabled);
//        CGRect frame = view.frame;
//        frame.size.width -= 40;
//        frame.origin.x += 20;
//        view.frame = frame;
    }

//    UIView *infoWindow = [self mapView:self.mapView markerInfoWindow:self.pointMarkers.firstObject];
//    if(infoWindow == nil) {
//        DebugLog(@"view is nil");
//    }
//    else {
//        DebugLog(@"view type: %@", [[infoWindow class] description]);
//    }
    
    
//    CGRect frame = view.frame;
//    frame.origin.y -= 30;
//    view.frame = frame;
}

#else

- (void)mapViewRegionDidChange:(RMMapView *)mapView
{
    // Check if zooming..
    static float recentZoomLevel = 21;  // max zooom
    float currentZoomLevel = mapView.zoom;
    if(currentZoomLevel == recentZoomLevel) {
        // zoom is changed. must update clusterized objects
        self.isZoomChanged = NO;
    }
    else {
        self.isZoomChanged = YES;
    }
    recentZoomLevel = currentZoomLevel;
//    DebugLog(@"current zoom: %g", currentZoomLevel);

    [self updateUserLocationDistance];

    if(!self.isZoomChanged) {
        return;
    }

    if(currentZoomLevel > POINT_CLUSTERIZATION_LEVEL) {
        if(self.mapView.clusteringEnabled)
            self.mapView.clusteringEnabled = NO;
    }
    else if(!self.mapView.clusteringEnabled) {
        self.mapView.clusteringEnabled = YES;
    }
}


- (void)afterMapZoom:(RMMapView *)map byUser:(BOOL)wasUserAction
{
//    DebugLog(@"zoom");
    if(map.zoom > SWITCH_ZOOM_LEVEL && !self.isPointMode) {
        [self switchToPointsMode];
    }
    else if(map.zoom < SWITCH_ZOOM_LEVEL && self.isPointMode) {
        [self switchToClustersMode];
    }
}

- (RMMapLayer*)mapView:(RMMapView *)mapView layerForAnnotation:(RMAnnotation *)annotation
{
    RMMapLayer *layer = nil;
    if (annotation.isUserLocationAnnotation) {
        return layer;
    }

    UIImage *pinImage = self.isPointMode ? self.mainPinImage : self.clusterPinImage;

    if (annotation.isClusterAnnotation)
//    if (annotation.clusteringEnabled)
    {
        pinImage = self.clusterPinImage;
        layer = [[RMMarker alloc] initWithUIImage:pinImage];
        layer.opacity = 0.75;
        layer.bounds = CGRectMake(0, 0, pinImage.size.width, pinImage.size.height);

//        [(RMMarker *)layer setTextForegroundColor:[UIColor whiteColor]];
//        [(RMMarker *)layer changeLabelUsingText:[NSString stringWithFormat:@"%i", [annotation.clusteredAnnotations count]]];
    }
    else
    {
        layer = [[RMMarker alloc] initWithUIImage:pinImage];
        layer.bounds = CGRectMake(0, 0, pinImage.size.width, pinImage.size.height);
        layer.opacity = 1;
        layer.canShowCallout = YES;
    }

    return layer;
}

- (void)mapView:(RMMapView *)mapView didSelectAnnotation:(RMAnnotation *)annotation
{
    if(self.isPointMode) {
//        if (!annotation.isClusterAnnotation)
//        {
//            UIImage *pinImage = self.yellowPinImage;
////            RMMapLayer *layer = annotation.layer;
////            layer = [[RMMarker alloc] initWithUIImage:pinImage];
////            layer.bounds = CGRectMake(0, 0, pinImage.size.width, pinImage.size.height);
////            annotation.layer.backgroundColor = [UIColor redColor].CGColor;
//            annotation.layer.contents = (id)pinImage;
//            [CATransaction flush];
////            annotation.layer = layer;
////            layer.canShowCallout = YES;
//        }
    }
    else {
        if(annotation.isClusterAnnotation) {
            return;
        }

        NSNumber *clusterID = annotation.userInfo;
        WMCluster *cluster = [WMDBLayer clusterWithId:clusterID.intValue];
        if(cluster == nil) {
            DebugLog(@"no cluster '%@' found in DB", clusterID);
            return;
        }

        if(cluster.isPointsUpdated.boolValue) {
            // load poitns
            DebugLog(@"cluster '%@' points are already updated!", clusterID);
            return;
        }

        [WMPointsHandler getPointsForClusterId:clusterID.intValue];
    }
    
}

- (void)tapOnLabelForAnnotation:(RMAnnotation *)annotation onMap:(RMMapView *)map
{
    if(!self.isPointMode) {
        return;
    }
    NSString *identifier = annotation.userInfo;
    WMPoint *point = [WMDBLayer pointWithId:identifier];
    self.pointDetailsController.point = point;

    if(IS_IPAD) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"showPointDetails" object:nil userInfo:nil];
    }
    else {
        [self.navigationController pushViewController:self.pointDetailsController animated:YES];
    }
}


- (void)singleTapOnMap:(RMMapView *)map at:(CGPoint)point
{
    if(map.selectedAnnotation == nil) {
        [self switchSearchBarVisibility];
    }
}


#endif

- (void)nearbyButtonTapped
{
    [self showNearby];
}

#pragma mark - private

- (void)initMapView
{
    self.isMapInited = NO;

#if LITE_VERSION
    [self initGoogleMaps];
#else
    [self initMapbox];
#endif

    [self.view addSubview:self.locationButton];
    [self.view bringSubviewToFront:self.locationButton];
    [self updateUserLocationDistance];
}

- (NSArray*)allClusters
{
    if(!_allClusters) {
        _allClusters = [[NSArray alloc] init];
    }
    return _allClusters;
}

- (void)setAllClusters:(NSArray *)visibleClusters
{
//    self.visiblePoints = nil;
    _allClusters = [NSArray arrayWithArray:visibleClusters];
}

- (NSArray*)visiblePoints
{
    if(!_visiblePoints) {
        _visiblePoints = [[NSArray alloc] init];
    }
    return _visiblePoints;
}

- (void)setVisiblePoints:(NSArray*)visiblePoints
{
    _visiblePoints = [NSArray arrayWithArray:visiblePoints];
}

- (NSMutableArray*)clusterMarkers
{
    if(!_clusterMarkers) {
        _clusterMarkers = [NSMutableArray array];
    }
    return _clusterMarkers;
}

- (NSMutableArray*)pointMarkers
{
    if(!_pointMarkers) {
        _pointMarkers = [NSMutableArray array];
    }
    return _pointMarkers;
}

- (void)fetchClustersFromDB
{
    self.allClusters = [WMDBLayer allClusters];

    if(self.allClusters.count == 0) {
        // this is very first app launch..
        self.loadIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.view addSubview:self.loadIndicator];
        [self.view bringSubviewToFront:self.loadIndicator];
        [self.loadIndicator startAnimating];
        
        CGRect frame = self.loadIndicator.frame;
        self.loadIndicator.center = self.view.center;
        frame = self.loadIndicator.frame;
    }
    else if(self.loadIndicator != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.loadIndicator stopAnimating];
            [self.loadIndicator removeFromSuperview];
            self.loadIndicator = nil;
        });
    }

    [self refreshClusterMarkers];
}

- (void)fetchPointsFromDB:(NSNotification*)notification __deprecated
{
    return;
    int clusterID = [[notification.userInfo objectForKey:@"cluster_id"] intValue];
    self.visiblePoints = [WMDBLayer pointsForClusterID:clusterID];
    [self refreshPointMarkers];
}

- (void)switchSearchBarVisibility
{
    BOOL isVisible = !self.searchBar.isHidden;
    if(IS_IPAD) {
        if(self.searchController) {
            [self.searchController setActive:NO animated:YES];
            return;
        }
    }
    if(!isVisible) {
        self.searchBar.alpha = 0;
        self.searchBar.hidden = NO;
        self.nearbyButton.alpha = 0;
        self.nearbyButton.hidden = NO;
    }
    else {
//        self.nearbyButton.enabled = self.textField.enabled = NO;
    }

    return; // The search bar hiding is didsabled

    [UIView animateWithDuration:0.5f delay:0 options:(UIViewAnimationOptionCurveEaseOut)
                     animations:^{
                         self.nearbyButton.alpha = self.searchBar.alpha = isVisible ? 0 : 1;
                     } completion:^(BOOL done) {
                         if(done) {
                             self.nearbyButton.hidden = self.searchBar.hidden = isVisible;
                             self.nearbyButton.enabled = self.textField.enabled = YES;
                         }
                     }];
}


#if LITE_VERSION
// GOOGLE MAPS..

- (void)initGoogleMaps
{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:59.94245956647393 longitude:30.292739868164062 zoom:15];
//#warning TEST
//    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:59.94245956647393 longitude:30.292739868164062 zoom:8];

    self.mapView = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
//    self.view = _mapView;
    self.mapView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [self.view addSubview:self.mapView];
    self.mapView.delegate = self;
    self.mapView.myLocationEnabled = YES;
    self.mapView.settings.rotateGestures = NO;

    // wait until user location is inited..
//    [self performSelector:@selector(goToUserLocation) withObject:nil afterDelay:0.5];
    

//    [self fetchClustersFromDB];
}

- (void)goToUserLocation
{
    // enable in the first place to avoid calling this method twice
    self.isMapInited = YES;
    
//    CLLocationCoordinate2D userCoord = self.mapView.myLocation.coordinate;
    CLLocationCoordinate2D userCoord = self.locationManager.location.coordinate;
    if(userCoord.latitude != 0 && userCoord.longitude != 0) {
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:userCoord.latitude longitude:userCoord.longitude zoom:15];
        [self.mapView setCamera:camera];
    }

    if([GUtils isFirstRun]) {
        [self performSelector:@selector(requestFirstPoints) withObject:nil afterDelay:1];
    }
    else {
        [self requestFirstPoints];
    }

    [WMNearbyViewController sharedController].userCoord = userCoord;
}

/**
 Fetches all clusters from DB and creates
 markers for those
 */
- (void)refreshClusterMarkers
{
    // clear all markers
    if(_clusterMarkers && _clusterMarkers.count > 0) {
        for(GMSMarker *marker in _clusterMarkers) {
            marker.map = nil;
        }
        _clusterMarkers = nil;
    }
    _clusterMarkers = [NSMutableArray array];

    @synchronized(@"add_marker") {
        for(WMCluster *cluster in self.allClusters) {
            GMSMarker *annotation = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(cluster.lat.doubleValue,
                                                                                             cluster.lon.doubleValue)];
            annotation.icon = self.clusterPinImage;

            int pointsCount = cluster.points.count;
            NSString *pointStr = @"точек";
            switch(pointsCount) {
                case 1:
                    pointStr = @"точка";
                    break;
                case 2: case 3: case 4:
                    pointStr = @"точки";
                    break;
            }

            annotation.title = [NSString stringWithFormat:@"%d %@", pointsCount, pointStr];
            annotation.userData = cluster.identifier;
            [_clusterMarkers addObject:annotation];
        }
    }
}

- (void)refreshPointMarkers
{
    if(self.pointMarkers && _pointMarkers.count > 0) {
        for(GMSMarker *marker in _pointMarkers) {
            marker.map = nil;
        }
        _pointMarkers = nil;
    }
    
    _pointMarkers = [NSMutableArray array];
    
    DebugLog(@"----------- 1");
    for(WMPoint *point in self.visiblePoints) {
        GMSMarker *annotation = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(point.lat.doubleValue,
                                                                                         point.lon.doubleValue)];
        annotation.icon = self.mainPinImage;
        annotation.title = [NSString stringWithFormat:@"id: %@", point.title];
        annotation.userData = point.identifier;
//        annotation.appearAnimation = kGMSMarkerAnimationPop;
//        annotation.map = self.mapView;
        @synchronized(@"add_marker") {
            [_pointMarkers addObject:annotation];
        }
    }
    DebugLog(@"----------- 2");
    [self updateClusteringForCurrentView];
}

/**
 Initialize the screen grid. Free version only.
 */
- (void)initClusteringRegions
{
    _clusteringRegions = [NSMutableArray array];
    int cells = 32; // 8x4 or 4x8
    for(int c = 0; c < cells; ++c) {
        ClusterRegion *region = [ClusterRegion regionWithRect:CGRectZero];
        [_clusteringRegions addObject:region];
    }

    [self refreshClusteringRegionAreas];
}

/**
 Refreshes areas for clustering regions.
 Must be called after screen bounds has been changed.
 Will have no effect if clustering regions are not inited.
 */
- (void)refreshClusteringRegionAreas
{
    static UIInterfaceOrientation recentOrientation;
    UIInterfaceOrientation currentOrientation = self.interfaceOrientation;
    if(currentOrientation == recentOrientation || self.clusteringRegions == nil) {
        return;
    }

    int rows = 8;
    int columns = 4;
    CGRect frame = self.mapView.frame;
    frame = self.view.frame;
    CGSize cellSize = CGSizeMake(frame.size.width / columns, frame.size.height / rows);
    if(currentOrientation == UIInterfaceOrientationLandscapeLeft ||
       currentOrientation == UIInterfaceOrientationLandscapeRight) {
        rows = 4;
        columns = 8;
        cellSize = CGSizeMake(frame.size.height / columns, frame.size.width / rows);
    }
    int rowIndex = 0;
    int columnIndex = 0;

    for(ClusterRegion *region in self.clusteringRegions) {
        CGFloat x = columnIndex * cellSize.width;
        CGFloat y = rowIndex * cellSize.height;
        CGRect regionFrame = CGRectMake(x, y, cellSize.width, cellSize.height);
        ++columnIndex;
        if(columnIndex == columns) {
            columnIndex = 0;
            ++rowIndex;
        }
        region.area = regionFrame;

        if(DISPLAY_DEBUG_VIEWS) {
            UIView *v = region.bgView;
            if(!v) {
                v = [[UIView alloc] initWithFrame:regionFrame];
            }
            else {
                [v setFrame:regionFrame];
            }
            v.backgroundColor = [UIColor colorWithRed:arc4random() % 256 / 255.f
                                                green:arc4random() % 256 / 255.f
                                                 blue:arc4random() % 256 / 255.f
                                                alpha:0.2];
            UILabel *label = region.label == nil ? [[UILabel alloc] initWithFrame:CGRectZero] : region.label;
            label.font = [UIFont systemFontOfSize:12];
            label.textColor = [UIColor blackColor];
            [v addSubview:label];
            label.backgroundColor = [UIColor clearColor];
//                label.center = CGPointMake(CGRectGetMidX(v.bounds), CGRectGetMidY(v.bounds));
            label.frame = v.bounds;
            [self.view addSubview:v];
            v.userInteractionEnabled = NO;

            region.bgView = v;
            region.label = label;
        }
    }

    recentOrientation = currentOrientation;
}

/**
 Checks if clusters should be clusterized (grouped)
 and runs grouping if needed.
 */
- (void)updateClusteringForCurrentView
{
    GMSProjection *projection = self.mapView.projection;
//    CLLocationCoordinate2D topLeft = [projection coordinateForPoint:CGPointMake(0, 0)];
//    CLLocationCoordinate2D bottomRight = [projection coordinateForPoint:CGPointMake(CGRectGetMaxX(self.mapView.bounds),
//                                                                                    CGRectGetMaxY(self.mapView.bounds))];
//    UIImage *tMarkerImg = [GMSMarker markerImageWithColor:[UIColor redColor]];
//    UIImage *bMarkerImg = [GMSMarker markerImageWithColor:[UIColor blueColor]];
//    GMSMarker *tMarker = [GMSMarker markerWithPosition:topLeft];
//    GMSMarker *bMarker = [GMSMarker markerWithPosition:bottomRight];
//    tMarker.icon = tMarkerImg;
//    bMarker.icon = bMarkerImg;
//    tMarker.map = self.mapView;
//    bMarker.map = self.mapView;

    float clusteringEdge;
    NSArray *markers;

    if(self.isPointMode) {
        clusteringEdge = POINT_CLUSTERIZATION_LEVEL;
        markers = self.pointMarkers;
    }
    else {
        clusteringEdge = CLUSTER_CLUSTERIZATION_LEVEL;
        markers = self.clusterMarkers;
    }

    float zoom = self.mapView.camera.zoom;

    if(self.mapView.camera.zoom < clusteringEdge) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self clusterizeCurrentProjection:projection];
        });
        return;
    }

    // show only visible markers
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        NSTimeInterval time = [NSDate timeIntervalSinceReferenceDate];
        @synchronized(@"add_marker") {
            for(GMSMarker *marker in markers) {
                BOOL shouldRemove = ![projection containsCoordinate:marker.position];
                if(shouldRemove) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        marker.map = nil;
                    });
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        marker.map = self.mapView;
                    });
                }
            }
        }
//        DebugLog(@"time = %g", [NSDate timeIntervalSinceReferenceDate] - time);
    });
}

- (void)updatePointsForCurrentView
{
}

/**
 Runs over all the cluster markers, shows markers that appear within
 map projection area and hides all the others
 */
- (void)clusterizeCurrentProjection:(GMSProjection*)projection
{
    // collect visible markers
    NSMutableArray *visibleMarkers = [NSMutableArray array];
    NSMutableArray *allMarkers;
    if(self.isPointMode) {
        allMarkers = self.pointMarkers;
    }
    else {
        allMarkers = self.clusterMarkers;
    }

    for(GMSMarker *marker in allMarkers) {
        if([projection containsCoordinate:marker.position]) {
            [visibleMarkers addObject:marker];
        }
        else {
            marker.isGrouped = NO;
            marker.isKey = NO;
            marker.clusterRegion = nil;
            dispatch_async(dispatch_get_main_queue(), ^{
                marker.map = nil;
            });
        }

        if(self.isZoomChanged) {
            marker.isGrouped = NO;
            marker.isKey = NO;
            marker.clusterRegion = nil;
            dispatch_async(dispatch_get_main_queue(), ^{
                marker.map = nil;
            });
        }
        else {

        // hide all invisible markers
        if(!marker.isKey) {
            dispatch_async(dispatch_get_main_queue(), ^{
                marker.map = nil;
            });
        }
        }
    }

    // clear markers from all cluster regions
    for(ClusterRegion *region in self.clusteringRegions) {
        [region removeAllMarkers];
    }

    for(GMSMarker *marker in visibleMarkers) {
        CGPoint markerScreenPoint = [projection pointForCoordinate:marker.position];

        for(ClusterRegion *region in self.clusteringRegions) {

            // Check regions with marker-on-screen projections
            if([region involvesScreenPoint:markerScreenPoint]) {
                if(!marker.isGrouped) {
                    [region addMarker:marker];
                    marker.isGrouped = YES;
                    marker.isKey = NO;
                }
            }
        }
    }

    for(ClusterRegion *region in self.clusteringRegions) {
        if(region.groupMarker != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                region.groupMarker.map = self.mapView;
            });
        }
        [region displayDebugData];
    }
}

- (void)hideClusters
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // clear markers from all cluster regions
        for(ClusterRegion *region in self.clusteringRegions) {
            [region removeAllMarkers];
        }

        for(GMSMarker *marker in self.clusterMarkers) {
            marker.isKey = NO;
            marker.isGrouped = NO;
            marker.clusterRegion = nil;
//            dispatch_async(dispatch_get_main_queue(), ^{
//                marker.map = nil;
//            });
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mapView clear];
        });
    });
}

- (void)hidePoints
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // clear markers from all cluster regions
        for(ClusterRegion *region in self.clusteringRegions) {
            [region removeAllMarkers];
        }

        for(GMSMarker *marker in self.pointMarkers) {
            marker.isKey = NO;
            marker.isGrouped = NO;
            marker.clusterRegion = nil;
//            dispatch_async(dispatch_get_main_queue(), ^{
//                marker.map = nil;
//            });
        }
        self.pointMarkers = nil;
        self.visiblePoints = nil;

        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mapView clear];
        });
    });
}



#else
// MAPBOX..

- (void)initMapbox
{
//    self.mapView = [[RMMapView alloc] initWithFrame:self.view.bounds];
//    RMMapboxSource *mbs = [[RMMapboxSource alloc] initWithMapID:kMAPBOX_API_KEY enablingDataOnMapView:_mapView];
//    [_mapView setTileSource:mbs];

//    NSError *error = nil;
//    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:kMAPBOX_API_KEY ofType:@"json"];
//    NSString* tileJSON = [NSString stringWithContentsOfFile:jsonPath encoding:NSASCIIStringEncoding error:&error];
    NSString* tileJSON = [GUtils jsonForKey:[GUtils mapBoxKey]];
    RMMapboxSource *tileSource = [[RMMapboxSource alloc] initWithTileJSON:tileJSON];
    [tileSource setCacheable:YES];
    self.mapView = [[RMMapView alloc] initWithFrame:self.view.bounds];
    [_mapView setTileSource:tileSource];

    _mapView.showsUserLocation = YES;
    [self.view addSubview:self.mapView];
    [_mapView setCenterCoordinate:CLLocationCoordinate2DMake(59.94245956647393, 30.292739868164062)];
    [_mapView setZoom:5];
    _mapView.tileCache.backgroundCacheDelegate = self;
    _mapView.delegate = self;

    self.mapView.clusteringEnabled = YES;
    self.mapView.clusterAreaSize = CGSizeMake(100, 100);

    self.mapView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);

    // wait until user location is inited..
//    [self performSelector:@selector(goToUserLocation) withObject:nil afterDelay:0.2f];

//    if([GUtils isFirstRun]) {
//        [self performSelector:@selector(fetchClustersFromDB) withObject:nil afterDelay:0.5f];
//        [self performSelector:@selector(requestFirstPoints) withObject:nil afterDelay:1];
//    }
//    else {
//        [self fetchClustersFromDB];
//        [self requestFirstPoints];
//    }
}

- (void)goToUserLocation
{
    // enable in the first place to avoid calling this method twice
    self.isMapInited = YES;

//    CLLocationCoordinate2D userCoord = self.mapView.userLocation.coordinate;
    CLLocationCoordinate2D userCoord = self.locationManager.location.coordinate;
    DebugLog(@"user location: %g:%g", userCoord.latitude, userCoord.longitude);
    [self.mapView setCenterCoordinate:userCoord animated:NO];
    [self.mapView  setZoom:15];
    
    if([GUtils isFirstRun]) {
        [self performSelector:@selector(fetchClustersFromDB) withObject:nil afterDelay:0.5f];
        [self performSelector:@selector(requestFirstPoints) withObject:nil afterDelay:1];
    }
    else {
        [self fetchClustersFromDB];
        [self requestFirstPoints];
    }

    [WMNearbyViewController sharedController].userCoord = userCoord;
}


/**
 Fetches all clusters from DB and creates
 markers for those
 */
- (void)refreshClusterMarkers
{
    // clear all markers
    if(self.clusterMarkers && self.clusterMarkers.count > 0) {
//        [self.mapView removeAnnotations:_clusterMarkers];
//        _clusterMarkers = nil;
        [self.mapView addAnnotations:self.clusterMarkers];
        return;
    }
    _clusterMarkers = [NSMutableArray array];
    
    for(WMCluster *cluster in self.allClusters) {
//        RMPointAnnotation *annotation = [[RMPointAnnotation alloc] initWithMapView:self.mapView
//                                                                        coordinate:CLLocationCoordinate2DMake(cluster.lat.doubleValue,
//                                                                                                              cluster.lon.doubleValue)
//                                                                          andTitle:[NSString stringWithFormat:@"mmmm %@", cluster.identifier]];
        int pointsCount = cluster.points.count;
        NSString *pointStr = @"точек";
        switch(pointsCount) {
            case 1:
                pointStr = @"точка";
                break;
            case 2: case 3: case 4:
                pointStr = @"точки";
                break;
        }
        RMAnnotation *annotation = [[RMAnnotation alloc] initWithMapView:self.mapView
                                                                        coordinate:CLLocationCoordinate2DMake(cluster.lat.doubleValue,
                                                                                                              cluster.lon.doubleValue)
                                                                          andTitle:[NSString stringWithFormat:@"%d %@", pointsCount, pointStr]];
//        [annotation setImage:self.clusterPinImage];
        annotation.userInfo = cluster.identifier;
//        annotation.isCloud = YES;
//        annotation.clusteringEnabled = YES;
        [_clusterMarkers addObject:annotation];
    }

    [self.mapView addAnnotations:_clusterMarkers];
}


/**
 Fetches all clusters from DB and creates
 markers for those
 */
- (void)refreshPointMarkers
{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    // clear all markers
    if(_pointMarkers && _pointMarkers.count > 0) {
        [self.mapView removeAnnotations:_pointMarkers];
        _pointMarkers = nil;
    }
    _pointMarkers = [NSMutableArray array];

    for(WMPoint *point in self.visiblePoints) {
        RMAnnotation *annotation = [RMAnnotation annotationWithMapView:self.mapView
                                                            coordinate:CLLocationCoordinate2DMake(point.lat.doubleValue,
                                                                                                  point.lon.doubleValue)
                                                              andTitle:point.title];
        annotation.userInfo = point.identifier;
        [_pointMarkers addObject:annotation];
    }

//        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mapView addAnnotations:_pointMarkers];
//        });
//    });
}

- (void)hideClusters
{
    [self.mapView removeAnnotations:self.clusterMarkers];
}

- (void)hidePoints
{
    [self.mapView removeAnnotations:self.pointMarkers];
}

#endif


#pragma mark - PLACES
#pragma mark - SearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length < 1) {
        self.searchBar.text = @" ";
        return;
    }
    
//    [self startAnimating];
    NSString *urlStr = @"https://maps.googleapis.com/maps/api/place/autocomplete/json?";
    urlStr = [urlStr stringByAppendingFormat:@"input=%@&types=geocode&sensor=true&key=%@", searchText, kPLACES_API_KEY];
    urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if(connectionError) {
                                   NSLog(@"Connection error: %@", connectionError.userInfo);
//                                   dispatch_async(dispatch_get_main_queue(), ^{
//                                       [self stopAnimating];
//                                   });
                                   return;
                               }
                               
                               NSError *error = nil;
                               NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                               
                               if(error || ![[jsonResult objectForKey:@"status"] isEqualToString:@"OK"]) {
                                   NSLog(@"JSON data contains error: %@", error.userInfo);
//                                   dispatch_async(dispatch_get_main_queue(), ^{
//                                       [self stopAnimating];
//                                   });
                                   return;
                               }
                               
                               self.searchResluts = [jsonResult objectForKey:@"predictions"];
                               dispatch_async(dispatch_get_main_queue(), ^{
//                                   UISearchDisplayController *searchController = !IS_IPAD ? self.searchDisplayController : [self.childViewControllers.firstObject searchDisplayController];
//                                   [searchController.searchResultsTableView reloadData];
                                   [self.searchController.searchResultsTableView reloadData];
                               });
                           }];
}

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    if(!self.dimmView) {
        UIView * s = controller.searchResultsTableView.superview;
        Class viewType = NSClassFromString(@"_UISearchDisplayControllerDimmingView");
        
        if(!IS_IOS7) {
            s = controller.searchBar.superview;
            viewType = [UIControl class];
        }
        
        for(UIView *v in s.subviews) {
            if([v isKindOfClass:viewType]) {
                self.dimmView = v;
            }
        }
    }
//    [self.view addSubview:self.dimmView];
//    [self.view sendSubviewToBack:self.dimmView];
    [self.dimmView.superview addSubview:controller.searchResultsTableView];
    [self.dimmView.superview bringSubviewToFront:controller.searchResultsTableView];

    // --
    if(IS_IPAD) {
        [self setSearchViewActive:YES];
    }

    UIView *textFieldWhiteBG = [self.searchBar viewWithTag:0xAA];
    if(textFieldWhiteBG) {
        CGRect frame = textFieldWhiteBG.frame;
        if(!IS_IOS7 && IS_IPAD && NO /*disabled for now*/) {
            // IDIOTS!!
            BOOL isPortrait = (self.interfaceOrientation == UIInterfaceOrientationPortrait ||
                               self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
            frame.size.width = isPortrait ? self.view.frame.size.width : self.view.frame.size.height;
            frame.size.height = isPortrait ? self.view.frame.size.height : self.view.frame.size.width;
        }
        else {
            frame.size.width = self.view.frame.size.width;
            frame.size.height = self.view.frame.size.height;
        }
        frame.origin = CGPointMake(0, -20);
        [UIView animateWithDuration:0.01f animations:^{
            textFieldWhiteBG.frame = frame;
        }];
    }

    if(!IS_IOS7) {
        UIView *textFieldBlueBG = [self.searchBar viewWithTag:0xAB];
        if(textFieldBlueBG) {
            textFieldBlueBG.hidden = YES;
        }
    }
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller
{
    UIView *textFieldBlueBG = [self.searchBar viewWithTag:0xAB];
    if(textFieldBlueBG) {
        [self.searchBar bringSubviewToFront:textFieldBlueBG];
        [self.searchBar bringSubviewToFront:self.textField];
        
        if(IS_IOS7) {
            CGRect frame = self.textField.frame;
            frame.size.width = self.textField.frame.size.width;
            textFieldBlueBG.frame = frame;
        }
    }

    if(!self.dimmView) {
        UIView * s = controller.searchResultsTableView.superview;
        Class viewType = NSClassFromString(@"_UISearchDisplayControllerDimmingView");

        if(!IS_IOS7) {
            s = controller.searchBar.superview;
            viewType = [UIControl class];
        }

        for(UIView *v in s.subviews) {
            if([v isKindOfClass:viewType]) {
                self.dimmView = v;
            }
        }
    }

    [self.dimmView.superview addSubview:controller.searchResultsTableView];
    [self.dimmView.superview bringSubviewToFront:controller.searchResultsTableView];

    if(self.searchBar.text.length > 0) {
//        [controller.searchBar setText:self.searchBar.text];
//        [controller.searchResultsTableView reloadData];
    }
    else {
        self.searchBar.text = @" ";
    }
    [controller.searchBar setText:self.searchBar.text];
    [controller.searchResultsTableView reloadData];
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    UIView *textFieldWhiteBG = [self.searchBar viewWithTag:0xAA];
    if(textFieldWhiteBG) {
        CGRect frame = textFieldWhiteBG.frame;
        frame.size.width = self.view.frame.size.width;
        frame.size.height = 0;
        frame.origin = _textField.center;
        [UIView animateWithDuration:0.02f animations:^{
            textFieldWhiteBG.frame = frame;
        }];
    }

    // --
    if(IS_IPAD) {
        [self setSearchViewActive:NO];
    }

    if(!IS_IOS7) {
        return;
    }
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    UIView *textFieldBlueBG = [self.searchBar viewWithTag:0xAB];
    if(!IS_IOS7) {
        if(textFieldBlueBG) {
            textFieldBlueBG.hidden = NO;
        }
    }
    else {
        CGRect frame = textFieldBlueBG.frame;
        frame.size.width = self.textField.frame.size.width;
        textFieldBlueBG.frame = frame;
    }
}

#pragma mark - TableView Delegate and Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger count = self.searchResluts.count;
    if(count == 0) {
        tableView.separatorColor = [UIColor clearColor];
        return 1;
    }

    tableView.separatorColor = [UIColor lightGrayColor];
    return count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];

    if(indexPath.row == self.searchResluts.count) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }

    cell.selectionStyle = UITableViewCellSelectionStyleGray;

    NSDictionary *result = [self.searchResluts objectAtIndex:indexPath.row];
    if(!result) {
        return cell;
    }

    cell.textLabel.text = [result objectForKey:@"description"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int index = indexPath.row;
    if(index == self.searchResluts.count) {
        return;
    }

    NSAssert(index < self.searchResluts.count, @"tapped cell index is out of searched results bounds");
    
    NSString *placeName = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
    NSString *placeID = [[self.searchResluts objectAtIndex:index] objectForKey:@"reference"];
    [self startAnimating];

//    [self.searchDisplayController setActive:NO animated:YES];
//    if(IS_IPAD) {
//        [[self.childViewControllers.firstObject searchDisplayController] setActive:NO animated:YES];
////        CGRect frame = self.testView.frame;
////        frame.size.height = self.searchBar.frame.size.height;
////        self.testView.frame = frame;
//    }

    [self.searchBar setText:placeName];
    [self requestForPlaceDetails:placeID completionHandler:^(CLLocationCoordinate2D coordinate) {
        [self switchToClustersMode];
#if LITE_VERSION
        [self.mapView animateToZoom:9];
        [self.mapView animateToLocation:coordinate];
#else
        self.mapView.zoom = 9;
        [self.mapView setCenterCoordinate:coordinate animated:YES];
#endif
    }];

    [self.searchController setActive:NO animated:YES];
}



#pragma mark - Private

- (void)initSearchView
{
    if(IS_IPAD) {
        self.testView.hidden = YES;
        self.testView.clipsToBounds = YES;
        self.testView.layer.masksToBounds = YES;
        self.testView.layer.cornerRadius = 15;
//        self.testView.backgroundColor = [UIColor clearColor];
        [self.view bringSubviewToFront:self.testView];
        WMPlacesViewController *pvc = [[WMPlacesViewController alloc] initWithNibName:@"WMPlacesViewController" bundle:nil];
        [self addChildViewController:pvc];
        [self.testView addSubview:pvc.view];
        self.searchController = pvc.searchDisplayController;

        pvc.searchDisplayController.delegate = self;
        pvc.searchDisplayController.searchResultsDataSource = self;
        pvc.searchDisplayController.searchResultsDelegate = self;
        pvc.searchBar.delegate = self;
        self.searchBar = pvc.searchBar;
        _textField = pvc.textField;
        _activityIndicator = pvc.activityIndicator;
        _searchIcon = pvc.searchIcon;
    }
    else if(IS_IOS7) {
        self.searchController = self.searchDisplayController;
        _textField = [((UIView*)[self.searchBar.subviews objectAtIndex:0]).subviews lastObject];
        CGRect frame = self.searchBar.frame;
        frame.origin.y += 12;
        self.searchBar.frame = frame;
    }
    else {
        self.searchController = self.searchDisplayController;
        for(UIView *v in self.searchBar.subviews) {
            if([v isKindOfClass:[UITextField class]]) {
                _textField = (UITextField*)v;
                break;
            }
        }

        self.searchBar.backgroundColor = [UIColor clearColor];
    }

    if(_textField == nil) {
        DebugLog(@"ERROR: couldn't get the text field of search bar!");
        return;
    }

    [self performSelector:@selector(createSearchBarSubviews) withObject:nil afterDelay:0];

    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _searchIcon = _textField.leftView;

    [self.view bringSubviewToFront:self.searchBar];
    if(!self.downloadView.isHidden) {
        [self.view bringSubviewToFront:self.downloadView];
    }

    self.searchBar.placeholder = NSLocalizedString(@"search", @"");
}

- (void)createSearchBarSubviews
{
    [self.searchBar setBackgroundImage:[UIImage imageNamed:@"transparent.png"]];
    _textField.layer.cornerRadius = 14;
    [_textField setClipsToBounds:YES];

    UIView *textFieldWhiteBG = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0)];
    textFieldWhiteBG.backgroundColor = [UIColor colorWithRed:247.f / 256 green:247.f / 256 blue:247.f / 256 alpha:1];
    textFieldWhiteBG.tag = 0xAA;
    [_searchBar addSubview:textFieldWhiteBG];
    textFieldWhiteBG.center = _textField.center;
    [self.searchBar sendSubviewToBack:textFieldWhiteBG];

    CGRect frame = self.textField.frame;
    UIView *textFieldBlueBG = [[UIView alloc] initWithFrame:frame];
    if(!IS_IOS7) {
        frame.size.width -= 4;
        frame.size.height -= 4;
        frame.origin.x += 2;
        frame.origin.y += 2;
        textFieldBlueBG.frame = frame;
    }
    textFieldBlueBG.backgroundColor = [UIColor whiteColor];
    textFieldBlueBG.tag = 0xAB;
    [self.searchBar addSubview:textFieldBlueBG];
    [self.searchBar sendSubviewToBack:textFieldBlueBG];
    textFieldBlueBG.layer.shadowColor = [UIColor colorWithRed:0 green:105.f / 256 blue:205.f / 256 alpha:1].CGColor;
    textFieldBlueBG.layer.shadowRadius = 4;
    textFieldBlueBG.layer.shadowOpacity = 1;
    textFieldBlueBG.layer.shadowOffset = CGSizeMake(0, 0);
    textFieldBlueBG.layer.cornerRadius = 14;

    textFieldBlueBG.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    textFieldWhiteBG.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);

    self.nearbyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.nearbyButton setImage:[UIImage imageNamed:@"nearby_button.png"] forState:UIControlStateNormal];
    [self.nearbyButton sizeToFit];
    frame = _nearbyButton.frame;
    frame.origin.y = textFieldBlueBG.frame.origin.y + textFieldBlueBG.frame.size.height;
    if(IS_IOS7 && !IS_IPAD) {
        frame.origin.y += [[UIApplication sharedApplication] statusBarFrame].size.height / 2 + 2;
    }
    frame.origin.x = textFieldBlueBG.frame.size.width - textFieldBlueBG.frame.origin.y - frame.size.width - 10;
    self.nearbyButton.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin);
    self.nearbyButton.frame = frame;
    [self.nearbyButton addTarget:self action:@selector(showNearby) forControlEvents:UIControlEventTouchUpInside];

    if(IS_IPAD) {
        [self.testView addSubview:self.nearbyButton];
        CGRect newFrame = [self.view convertRect:self.nearbyButton.frame fromView:self.testView];
        self.nearbyButton.frame = newFrame;
        [self.view addSubview:self.nearbyButton];
        [self.view bringSubviewToFront:self.testView];
        [self setSearchViewActive:NO];
        self.testView.hidden = NO;
    }
    else {
        [self.view addSubview:self.nearbyButton];
    }

    self.searchController.searchResultsTableView.backgroundColor = textFieldWhiteBG.backgroundColor;

    [self.view bringSubviewToFront:self.searchBar];

    if(!self.downloadView.isHidden) {
        [self.view bringSubviewToFront:self.downloadView];
    }
}

/**
 iPad only resizes the search view height so it doesn't interrupt
 the touches on map view if not active
 */
- (void)setSearchViewActive:(BOOL)active
{
    if(!IS_IPAD) {
        return;
    }

    CGRect frame = self.testView.frame;
    if(!active) {
        frame.size.height = self.searchBar.bounds.size.height;
    }
    else {
        CGSize initialSize = CGSizeMake(320, 380); // size of search view in portrait orientation
        frame.size.height = initialSize.height;
    }
    self.testView.frame = frame;
    [self.view bringSubviewToFront:self.testView];
}

- (void)showNearby
{
    WMNearbyViewController * nearby = [WMNearbyViewController sharedController];
//    NSNumber *maxDistance = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"MaxDistance"];
//    NSNumber *maxVisibleDistance = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"MaxVisibleDistance"];
//    nearby.maxDistance = maxDistance.doubleValue;
//    nearby.maxVisibleDistance = maxVisibleDistance.doubleValue;
#if LITE_VERSION
    nearby.userCoord = self.mapView.myLocation.coordinate;
#else
    nearby.userCoord = self.mapView.userLocation.coordinate;
#endif
    if(IS_IPAD) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"showNearby" object:self userInfo:nil];
    }
    else {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self.navigationController pushViewController:nearby animated:YES];
    }
}

- (void)startAnimating
{
    DebugLog(@"start animating");
    self.textField.leftView = self.activityIndicator;
    [self.activityIndicator startAnimating];
}

- (void)stopAnimating
{
    DebugLog(@"stop animating");
    [self.activityIndicator stopAnimating];
    self.textField.leftView = self.searchIcon;
}


- (void)requestForPlaceDetails:(NSString*)placeID completionHandler:(void (^)(CLLocationCoordinate2D coordinate))handler
{
    NSString *urlStr = @"https://maps.googleapis.com/maps/api/place/details/json?";
    urlStr = [urlStr stringByAppendingFormat:@"reference=%@&sensor=false&key=%@", placeID, kPLACES_API_KEY];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue new]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
    {
        NSError *error = nil;
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if(error || ![[result objectForKey:@"status"] isEqualToString:@"OK"]) {
            DebugLog(@"error getting place details. JSON error: %@", error ? error.userInfo : @"status != OK");
        }
        else {
            double lat = [[[[[result objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] doubleValue];
            double lon = [[[[[result objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"] doubleValue];
            if(handler) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(CLLocationCoordinate2DMake(lat, lon));
                });
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self stopAnimating];
        });

    }];
}


- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if(status == kCLAuthorizationStatusAuthorized && !self.isMapInited) {
        [self goToUserLocation];

        // check if the region is already being monitored..
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier = %@", kUSER_HOME_LOCATION];
        if([self.locationManager.monitoredRegions filteredSetUsingPredicate:predicate].count == 0) {
            [self updateUserHomeLocation];
        }
    }
    
    [self updateUserLocationDistance];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [self updateUserLocationDistance];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
//    UILocalNotification *notification = [[UILocalNotification alloc] init];
//    notification.alertBody = @"IN!!";
//    notification.alertAction = @"Ok";
//    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.userInfo = [NSDictionary dictionaryWithObject:@"user info!!" forKey:@"user_info"];
    notification.alertBody = NSLocalizedString(@"OUT", @"");
    notification.alertAction = @"Ok";
    notification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

- (void)updateUserHomeLocation
{
    // start monitoring current location..
    CLLocationCoordinate2D userCoord = self.locationManager.location.coordinate;
    CLRegion *region = [[CLRegion alloc] initCircularRegionWithCenter:userCoord radius:DISTANCE_FROM_HOME identifier:kUSER_HOME_LOCATION];
    [self.locationManager startMonitoringForRegion:region];
}

- (IBAction)locationButtonAction
{
#if LITE_VERSION
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:self.mapView.myLocation.coordinate zoom:17];
//    [self.mapView animateToZoom:19];
    self.mapView.camera = camera;
#else
    CLLocationCoordinate2D userCoord = self.mapView.userLocation.coordinate;
    self.mapView.zoom = 17;
    [self.mapView setCenterCoordinate:userCoord animated:NO];
#endif

    [self switchToPointsMode:40];
}

- (void)updateUserLocationDistance
{
    CGPoint center = CGPointMake(CGRectGetMidX(self.mapView.bounds), CGRectGetMidY(self.mapView.bounds));
    CLLocation *userLocation;
    CLLocationCoordinate2D currentCoord;
#if LITE_VERSION
    userLocation = self.mapView.myLocation;
    currentCoord = [self.mapView.projection coordinateForPoint:center];
#else
    userLocation = [[CLLocation alloc] initWithLatitude:self.mapView.userLocation.coordinate.latitude
                                              longitude:self.mapView.userLocation.coordinate.longitude];
    currentCoord = [self.mapView pixelToCoordinate:center];
#endif

//    CLLocationDistance = userLocation - self.mapView.my

    CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:currentCoord.latitude longitude:currentCoord.longitude];
    CLLocationDistance distance = fabsf([currentLocation distanceFromLocation:userLocation]) / 1000.f;
    NSString *buttonTitle;
    if(distance < 1) {
        if(distance < 0.1) {
            buttonTitle = @"0";
        }
        else {
            buttonTitle = [NSString stringWithFormat:@"%.01f", distance];
        }
    }
    else {
        buttonTitle = [NSString stringWithFormat:@"%d", (int)distance];
    }
    [self.locationButton setTitle:buttonTitle forState:UIControlStateNormal];
}


#pragma mark - Caching

- (IBAction)saveCurrentMap
{
#if !LITE_VERSION
    if(self.isDownloading) {
        return;
    }

    CGRect area = self.mapView.bounds;
    CLLocationCoordinate2D southWest = [self.mapView pixelToCoordinate:CGPointMake(0, CGRectGetMaxY(area))];
    CLLocationCoordinate2D northEast = [self.mapView pixelToCoordinate:CGPointMake(CGRectGetMaxX(area), 0)];
    float min = fmaxf(self.mapView.zoom - kDOWNLOAD_ZOOM_MIN, 0);
    float max = fminf(self.mapView.zoom + kDOWNLOAD_ZOOM_MAX, 19);
    [self.mapView.tileCache beginBackgroundCacheForTileSource:self.mapView.tileSource southWest:southWest
                                                    northEast:northEast
                                                      minZoom:min
                                                      maxZoom:max];
    self.downloadOkButton.enabled = NO;
    self.downloadViewTapGesture.enabled = NO;
    self.isDownloading = YES;

    self.downloadProgressBar.hidden = NO;
    self.downloadProgressLabel.hidden = NO;
    self.downloadCancelButton.hidden = YES;
    self.downloadOkButton.hidden = YES;
    self.downloadProgressBar.progress = 0;
    self.downloadProgressLabel.text = [NSString stringWithFormat:@"%@ 0%%", NSLocalizedString(@"downloading", @"")];

#else
    NSURL *url = [NSURL URLWithString:kPRO_URL];
    [[UIApplication sharedApplication] openURL:url];
#endif
}

#if !LITE_VERSION

- (void)tileCache:(RMTileCache *)tileCache didBeginBackgroundCacheWithCount:(int)tileCount forTileSource:(id<RMTileSource>)tileSource
{
    
}

- (void)tileCache:(RMTileCache *)tileCache didBackgroundCacheTile:(RMTile)tile withIndex:(int)tileIndex ofTotalTileCount:(int)totalTileCount
{
    float progress = 1.f * tileIndex / totalTileCount;
    self.downloadProgressBar.progress = progress;
    self.downloadProgressLabel.text = [NSString stringWithFormat:@"%@ %d%@", NSLocalizedString(@"downloading", @""), (int)(progress * 100), @"%"];
}

- (void)tileCacheDidCancelBackgroundCache:(RMTileCache *)tileCache
{
    self.downloadOkButton.enabled = YES;
    self.downloadViewTapGesture.enabled = YES;
    self.isDownloading = NO;
    self.downloadProgressBar.hidden = YES;
    self.downloadProgressLabel.hidden = YES;
    self.downloadCancelButton.hidden = NO;
    self.downloadOkButton.hidden = NO;
    [self closeDownloadView];
}

- (void)tileCacheDidFinishBackgroundCache:(RMTileCache *)tileCache
{
    self.downloadOkButton.enabled = YES;
    self.downloadViewTapGesture.enabled = YES;
    self.isDownloading = NO;
    self.downloadProgressBar.hidden = YES;
    self.downloadProgressLabel.hidden = YES;
    self.downloadCancelButton.hidden = NO;
    self.downloadOkButton.hidden = NO;
    [self closeDownloadView];
}

- (NSString*)savedDBPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dbPath = [cachePath stringByAppendingPathComponent:@"/RMTileCache.db"];
    if([fileManager fileExistsAtPath:dbPath]) {
        return dbPath;
    }
    return @"";
}

#endif
#pragma mark - Download

- (IBAction)closeDownloadView
{
    if(self.isDownloading) {
        return;
    }
    [self closeDownloadView:YES];
}

- (IBAction)showDownloadView
{
    [self showDownloadView:YES];
}

- (IBAction)cancelDownload
{
#if !LITE_VERSION
    if(self.isDownloading) {
        [self.mapView.tileCache cancelBackgroundCache];
    }
    else {
        [self closeDownloadView];
    }
#else
    [self closeDownloadView];
#endif
}

- (void)showDownloadView:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kBLOCK_ORIENTATION_CHANGE object:self];

//    if(self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft || self.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
//        self.downloadView.transform = CGAffineTransformMakeRotation(M_PI_2);
//        CGRect frame = self.downloadView.bounds;
//        frame.size = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height);
//        self.downloadView.frame = frame;
//    }

    [self.view bringSubviewToFront:self.downloadView];

#if !LITE_VERSION
    self.downloadProgressBar.hidden = YES;
    self.downloadProgressLabel.hidden = YES;
    self.downloadCancelButton.hidden = NO;
    self.downloadOkButton.hidden = NO;
#endif

    self.downloadScrollView.contentOffset = CGPointZero;
    self.downloadPageControl.currentPage = 0;

    if(!animated) {
        self.downloadView.hidden = NO;
        self.downloadView.backgroundColor = [UIColor colorWithWhite:0.1f alpha:0.7f];
        return;
    }
    
    self.downloadView.hidden = YES;

    CGRect topFrame = self.downloadTopView.frame;
    topFrame.origin.y = -1 * topFrame.size.height;
    CGRect bottomFrame = self.downloadBottomView.frame;
    bottomFrame.origin.y = self.view.frame.size.height;
#if LITE_VERSION
    if(self.interfaceOrientation == UIInterfaceOrientationLandscapeRight || self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        bottomFrame.origin.y = self.view.frame.size.width;
    }
#endif

    self.downloadTopView.frame = topFrame;
    self.downloadBottomView.frame = bottomFrame;

    topFrame.origin.y = 0;
    bottomFrame.origin.y = self.view.frame.size.height - bottomFrame.size.height;
#if LITE_VERSION
    if(self.interfaceOrientation == UIInterfaceOrientationLandscapeRight || self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        bottomFrame.origin.y = self.view.frame.size.width - bottomFrame.size.height;
    }
#endif
    self.downloadView.backgroundColor = [UIColor colorWithWhite:0.1f alpha:0];

    self.downloadView.hidden = NO;

    [UIView animateWithDuration:0.3f delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.downloadTopView.frame = topFrame;
                         self.downloadBottomView.frame = bottomFrame;
                         self.downloadView.backgroundColor = [UIColor colorWithWhite:0.1f alpha:0.7f];
                     }
                     completion:^(BOOL finished){
                         if(finished) {
                         }
                     }];
}

- (void)closeDownloadView:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kUNBLOCK_ORIENTATION_CHANGE object:self];

    if(!animated) {
        [self.view sendSubviewToBack:self.downloadView];
        self.downloadView.hidden = YES;
        self.downloadView.backgroundColor = [UIColor colorWithWhite:0.1f alpha:0];
        return;
    }

    CGRect topFrame = self.downloadTopView.frame;
    topFrame.origin.y = -1 * topFrame.size.height;
    CGRect bottomFrame = self.downloadBottomView.frame;
    bottomFrame.origin.y = self.view.frame.size.height;
    if(self.interfaceOrientation == UIInterfaceOrientationLandscapeRight || self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        bottomFrame.origin.y = self.view.frame.size.width;
    }

    [UIView animateWithDuration:0.2f delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.downloadTopView.frame = topFrame;
                         self.downloadBottomView.frame = bottomFrame;
                         self.downloadView.backgroundColor = [UIColor colorWithWhite:0.1f alpha:0];
                     }
                     completion:^(BOOL finished){
                         [self.view sendSubviewToBack:self.downloadView];
                         self.downloadView.hidden = YES;
                     }];
}

- (void)initDownloadView
{
    self.downloadView.hidden = YES;

    UIEdgeInsets insets = UIEdgeInsetsMake(20, 30, 44, 32);

    UIImage *image = [self.downloadOkButton backgroundImageForState:UIControlStateNormal];
    image = [image resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch];
    [self.downloadOkButton setBackgroundImage:image forState:UIControlStateNormal];
    [self.downloadOkButton setTitle:NSLocalizedString(@"download", @"") forState:UIControlStateNormal];

    image = [self.downloadCancelButton backgroundImageForState:UIControlStateNormal];
    image = [image resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch];
    [self.downloadCancelButton setBackgroundImage:image forState:UIControlStateNormal];
    [self.downloadCancelButton setTitle:NSLocalizedString(@"cancel", @"") forState:UIControlStateNormal];

#if LITE_VERSION
    self.downloadInfoLabel.text = NSLocalizedString(@"download_info_lite", @"");
    self.downloadCancelButton.hidden = YES;
    CGRect frame = self.downloadOkButton.frame;
    frame.size.width *= 1.5f;
    self.downloadOkButton.frame = frame;
    self.downloadOkButton.center = CGPointMake(CGRectGetMidX(self.downloadOkButton.superview.bounds), CGRectGetMidY(self.downloadOkButton.superview.bounds));
    self.downloadOkButton.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
    [self.downloadOkButton setTitle:NSLocalizedString(@"download_pro", @"") forState:UIControlStateNormal];

    self.downloadMapImage.hidden = YES;
    NSUInteger imageCount = 4;
//    CGFloat shift = IS_IPAD ? 32 : 22;
    CGFloat shift = 0;
    for(int i = 0; i < imageCount; ++i) {
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"scroll_item%d.png", i+1]];
        CGRect frame = self.downloadScrollView.bounds;
//        frame.size.height *= 0.14f;
        frame.origin.y = shift;
        frame.origin.x = frame.size.width * i;
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:frame];
//        imgView.contentMode = UIViewContentModeScaleAspectFit;
        imgView.contentMode = UIViewContentModeTop;
        imgView.image = img;
        [self.downloadScrollView addSubview:imgView];

        // set the tags to rearrange these views later
        imgView.tag = i;
    }
    self.downloadInfoLabel.tag = 0;

    self.downloadPageControl.numberOfPages = imageCount;
    self.downloadScrollView.contentSize = CGSizeMake(self.downloadScrollView.frame.size.width * imageCount, 0);
    self.downloadScrollView.scrollEnabled = YES;
    self.downloadScrollView.pagingEnabled = YES;

    self.downloadProgressBar.hidden = YES;
    self.downloadProgressLabel.hidden = YES;

    CGFloat rotAngle = M_PI_2;
    switch (self.interfaceOrientation) {
        case UIInterfaceOrientationLandscapeLeft:
            rotAngle = M_PI_2;
            break;
        case UIInterfaceOrientationLandscapeRight:
            rotAngle = -1 * M_PI_2;
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            rotAngle = 0;
            break;
        default:
            rotAngle = 0;
            break;
    }
    
    self.downloadView.transform = CGAffineTransformMakeRotation(rotAngle);
    frame = self.downloadView.bounds;
    frame.size = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height);
    self.downloadView.frame = frame;

#else
    self.downloadInfoLabel.text = NSLocalizedString(@"download_info", @"");
    [self.downloadTopView addSubview:self.downloadInfoLabel];
    self.downloadInfoLabel.hidden = NO;
//    self.downloadInfoLabel.frame = CGRectOffset(self.downloadInfoLabel.frame, 0, 60);
    self.downloadInfoLabel.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
//    [self.downloadView bringSubviewToFront:self.downloadMidView];
    [self.downloadCancelButton addTarget:self action:@selector(cancelDownload) forControlEvents:UIControlEventTouchUpInside];

    self.downloadPageControl.hidden = YES;
    self.downloadScrollView.hidden = YES;
    self.downloadProgressBar.hidden = YES;
    self.downloadProgressLabel.hidden = YES;
    self.downloadCancelButton.hidden = NO;
    self.downloadOkButton.hidden = NO;
#endif
    
    self.isDownloading = NO;
}

- (void)initWifiInfoView
{
    self.wifiInfoView.hidden = YES;

    self.wifiInfoLabel.text = NSLocalizedString(@"wifi_info", @"");
    [self.addWifiButton setTitle:NSLocalizedString(@"add_wifi", @"") forState:UIControlStateNormal];
    [self.addWifiButton addTarget:self action:@selector(addCurrentWifiToDatabase) forControlEvents:UIControlEventTouchUpInside];
    [self.view bringSubviewToFront:self.wifiInfoView];
}

- (void)wifiStateChanged
{
    self.currentWifiName = [GUtils sharedUtils].currentWifiName;
}

- (NSString*)currentWifiName
{
    return _currentWifiName;
}

- (void)setCurrentWifiName:(NSString *)currentWifiName
{
    if(currentWifiName == nil) {
        _currentWifiName = currentWifiName;
        self.wifiInfoLabel.text = @"";
        [self setWiFiViewVisible:NO animated:YES];
    }
    else {
        self.wifiInfoLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"wifi_info", @""), currentWifiName];
        _currentWifiName = [NSString stringWithString:currentWifiName];

        [self setWiFiViewVisible:YES animated:YES];
    }
}

- (void)setWiFiViewVisible:(BOOL)visible animated:(BOOL)animated
{
    if(!animated) {
        self.wifiInfoView.hidden = !visible;
        return;
    }

    if(!visible) {
        self.wifiInfoLabel.text = @"";
        [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.wifiInfoView.alpha = 0;
                         }
                         completion:^(BOOL done) {
                             if(done) {
                                 self.wifiInfoView.hidden = YES;
                             }
                         }];
    }
    else {
        self.wifiInfoView.alpha = 0;
        self.wifiInfoView.hidden = NO;
        [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.wifiInfoView.alpha = 1;
                         }
                         completion:^(BOOL done) {
                         }];

        // hide the view after 15 seconds..
        [self performSelector:@selector(hideWifiView) withObject:nil afterDelay:5];
    }
//    CGRect frame = self.addWifiButton.frame;
//    frame.origin.y = self.wifiInfoLabel.frame.origin.y + self.wifiInfoLabel.frame.size.height - 15;
//    self.addWifiButton.frame = frame;
}

- (void)hideWifiView
{
    [self setWiFiViewVisible:NO animated:YES];
}

- (WMNewPointViewController*)newPointController
{
    if(!_newPointController) {
        _newPointController = [[WMNewPointViewController alloc] init];
        [_newPointController viewDidLoad];
    }
    return _newPointController;
}

- (void)addCurrentWifiToDatabase
{
    if(IS_IPAD) {
        if(IS_IPAD) {
            CLLocationCoordinate2D userCoord = self.locationManager.location.coordinate;
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.newPointController, @"controller",
                                      self.currentWifiName, @"point_name",
                                      [NSNumber numberWithDouble:userCoord.latitude], @"lat",
                                      [NSNumber numberWithDouble:userCoord.longitude], @"lon", nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showNewPoint" object:self userInfo:userInfo];
        }
    }
    else {
        [self.navigationController pushViewController:self.newPointController animated:YES];
        self.newPointController.name = self.currentWifiName;
        self.newPointController.coordinate = self.locationManager.location.coordinate;
    }
    DebugLog(@"Adding hotspot '%@' to DB..", self.currentWifiName);
}

- (void)rearrangeScrollViewForOrientation:(UIInterfaceOrientation)orientation
{
    if(!LITE_VERSION) {
        return;
    }

    CGFloat width;
    if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        width = fmaxf(self.view.bounds.size.width, self.view.bounds.size.height);
    }
    else {
        width = fminf(self.view.bounds.size.width, self.view.bounds.size.height);
    }

    CGRect frame;
    int maxTag = 0;
    for(UIView *item in self.downloadScrollView.subviews) {
        int tag = item.tag;
        frame = item.frame;
        frame.origin.x = tag * width + (width - item.bounds.size.width) / 2.f;
        item.frame = frame;

        maxTag = MAX(maxTag, tag);
    }

    CGSize contentSize = self.downloadScrollView.contentSize;
    contentSize.width = width * (maxTag + 1);
    self.downloadScrollView.contentSize = contentSize;
    self.downloadScrollView.contentOffset = CGPointZero;
    self.downloadPageControl.currentPage = 0;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
#if LITE_VERSION
    if(scrollView == self.downloadScrollView) {
        self.downloadPageControl.currentPage = (int)(scrollView.contentOffset.x / scrollView.frame.size.width);
    }
#endif
}

@end


















