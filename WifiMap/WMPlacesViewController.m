//
//  WMPlacesViewController.m
//  WifiMap
//
//  Created by Gor on 07/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//


//
// This View Controller is not used now, but may be useful for iPad UI (don't remove yet!)
//


#import "WMPlacesViewController.h"

@interface WMPlacesViewController ()
//@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
//@property (strong, nonatomic, readonly) UITextField *textField;
//@property (strong, nonatomic, readonly) UIActivityIndicatorView *activityIndicator;
//@property (strong, nonatomic, readonly) UIView *searchIcon;

@property (strong, nonatomic) NSArray *searchResluts;

@end

@implementation WMPlacesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        self.searchBar.delegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initViews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length < 1) {
        return;
    }

    [self startAnimating];
    NSString *urlStr = @"https://maps.googleapis.com/maps/api/place/autocomplete/json?";
    urlStr = [urlStr stringByAppendingFormat:@"input=%@&types=geocode&sensor=false&key=%@", searchText, kPLACES_API_KEY];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if(connectionError) {
                                   NSLog(@"Connection error: %@", connectionError.userInfo);
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       [self stopAnimating];
                                   });
                                   return;
                               }
                               
                               NSError *error = nil;
                               NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

                               if(error || ![[jsonResult objectForKey:@"status"] isEqualToString:@"OK"]) {
                                   NSLog(@"JSON data contains error: %@", error.userInfo);
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       [self stopAnimating];
                                   });
                                   return;
                               }

                               self.searchResluts = [jsonResult objectForKey:@"predictions"];
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   [self.searchDisplayController.searchResultsTableView reloadData];
                                   [self stopAnimating];
                               });
                           }];
}

#pragma mark - TableView Delegate and Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResluts.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }

    NSDictionary *result = [self.searchResluts objectAtIndex:indexPath.row];
    if(!result) {
        return cell;
    }
    
    cell.textLabel.text = [result objectForKey:@"description"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int index = indexPath.row;
    NSAssert(index < self.searchResluts.count, @"tapped cell index is out of searched results bounds");

//    NSString *placeName = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
//    NSString *placeID = [[self.searchResluts objectAtIndex:index] objectForKey:@"id"];
    [self startAnimating];

    [self dismissModalViewControllerAnimated:YES];
}



#pragma mark - Private

- (void)initViews
{
    if(IS_IOS7) {
        _textField = [((UIView*)[self.searchBar.subviews objectAtIndex:0]).subviews lastObject];
        CGRect frame = self.searchBar.frame;
//        frame.origin.y += 12;
        self.searchBar.frame = frame;
//        UIView *black = [((UIView*)[self.searchBar.subviews objectAtIndex:0]).subviews firstObject];
//        black.backgroundColor = [UIColor redColor];
//        self.searchDisplayController.
    }
    else {
        for(UIView *v in self.searchBar.subviews) {
            if([v isKindOfClass:[UITextField class]]) {
                _textField = (UITextField*)v;
                break;
            }
        }
        
        self.searchBar.backgroundColor = [UIColor clearColor];
    }

    if(_textField == nil) {
        DebugLog(@"ERROR: couldn't get the text field of search bar!");
        return;
    }

    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _searchIcon = _textField.leftView;
}

- (void)startAnimating
{
    DebugLog(@"start animating");
    self.textField.leftView = self.activityIndicator;
    [self.activityIndicator startAnimating];
}

- (void)stopAnimating
{
    DebugLog(@"stop animating");
    [self.activityIndicator stopAnimating];
    self.textField.leftView = self.searchIcon;
}

@end




















