//
//  WMTextView.m
//  WifiMap
//
//  Created by Gor on 23/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import "WMTextView.h"

@implementation WMTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)copy:(id)sender
{
    [super copy:sender];

    static BOOL isFirstTime = YES;
    if(isFirstTime) {
        NSString *title = NSLocalizedString(@"password_copied", @"");
        NSString *message = NSLocalizedString(@"password_copied_message", @"");
//        NSString *title = @"Пароль скопирован";
//        NSString *message = @"К сожалению мы не можем подключить Вас автоматически. Откройте Настройки в iOS и воспользуйтесь данной подсказкой от этого Wi-Fi для подключения к нему в ручную";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    isFirstTime = NO;
}

@end
