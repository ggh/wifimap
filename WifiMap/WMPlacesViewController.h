//
//  WMPlacesViewController.h
//  WifiMap
//
//  Created by Gor on 07/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WMPlacesViewController : UIViewController <UISearchBarDelegate, UISearchDisplayDelegate,
                                                      UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic, readonly) UITextField *textField;
@property (strong, nonatomic, readonly) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic, readonly) UIView *searchIcon;


@end
