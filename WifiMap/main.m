//
//  main.m
//  WifiMap
//
//  Created by Gor on 22/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WMAppDelegate class]));
    }
}
