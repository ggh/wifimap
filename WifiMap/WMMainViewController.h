//
//  WMViewController.h
//  WifiMap
//
//  Created by Gor on 22/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WMMainViewController : UIViewController/* <UISearchBarDelegate, UISearchDisplayDelegate,
                                                    UITableViewDataSource, UITableViewDelegate>*/

- (void)showDownloadForCurrentLocation;

@end
