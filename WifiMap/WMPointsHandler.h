//
//  WMPointsHandler.h
//  WifiMap
//
//  Created by Gor on 22/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#define nDID_UPDATE_CLUSTERS    @"custers_updated"
#define nDID_UPDATE_POINTS      @"points_updated"

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface WMPointsHandler : NSObject

//+ (WMPointsHandler*)sharedHandler;
+ (void)getClustersForArea:(CLLocationCoordinate2D)topLeft bottomRight:(CLLocationCoordinate2D)bottomRight;


/**
 Requests all the clusters from server and saves in DB in background
 It's meant to fetch all clusters from server only once in application
 lifetime, hence this method will request the clusters only if there are
 no locally saved clusters and won't do duplicate checks during saving
 */
+ (void)getAllClusters;

+ (void)getPointsForArea:(CLLocationCoordinate2D)topLeft bottomRight:(CLLocationCoordinate2D)bottomRight;


+ (void)getPointsForClusterId:(int)clusterId;

/**
 Must retrieve points cluster by cluster
 */
+ (void)getPointsForClusters:(NSArray*)clusters __deprecated_msg("Use getPointsForClusterId instead");

+ (void)getAllPoints;

+ (void)addNewPoint:(CLLocationCoordinate2D)coordinate name:(NSString*)name password:(NSString*)password onFinish:(void (^)(BOOL success))onFinish;

@end
