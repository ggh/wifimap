//
//  WMPointsHandler.m
//  WifiMap
//
//  Created by Gor on 22/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#define URL_FOR_CLUSTERS        @"http://5.175.192.184/?lat1=%f&lng1=%f&lat2=%f&lng2=%f&what=cluster"
#define URL_FOR_CLUSTER_POINTS  @"http://5.175.192.184/?cluster="
#define URL_FOR_POINTS          @"http://5.175.192.184/?lat1=%f&lng1=%f&lat2=%f&lng2=%f"
#define URL_FOR_NEW_POINT       @"http://5.175.192.184/?act=create&lat=%f&lng=%f&country=%@&state=%@&city=%@&address=%@&street=%@&kind=%@&title=%@&hours=%@&comment=%@&id=%@"
//#define URL_FOR_NEW_POINT       @"http://5.175.192.184/?act=create&lat=%f&lng=%f&title=%@&comment=%@";


#import "WMPointsHandler.h"
#import "WMDBLayer.h"

@interface WMPointsHandler()

@property (nonatomic, readonly, strong) NSOperationQueue *downloadQueue;

// TMP. TODO: Remove!
@property (nonatomic, strong) NSMutableArray* tmpClusters;
@property (nonatomic) int currentTmpClusterIndex;

/**
 A flag to avoid sending new requests for clusters.
 This is used mainly to avoid creation of duplicate
 clusters by sending new cluster requests while all
 clusters are being fetched from server (which runs
 only once in lifetime)
 In all other cases this flag should be set to NO
 */
@property BOOL clusterRequestsBlocked;

@end

@implementation WMPointsHandler

@synthesize downloadQueue = _downloadQueue;

+ (WMPointsHandler*)sharedHandler
{
    static WMPointsHandler *handler = nil;

    if(handler) {
        return handler;
    }

    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        handler = [[WMPointsHandler alloc] init];
    });

    return handler;
}

- (id)init
{
    self = [super init];
    if(self) {
        self.clusterRequestsBlocked = YES;
    }
    return self;
}

+ (void)getClustersForArea:(CLLocationCoordinate2D)topLeft bottomRight:(CLLocationCoordinate2D)bottomRight
{
    [[WMPointsHandler sharedHandler] getClustersForArea:topLeft bottomRight:bottomRight withDuplicateCheck:YES];
}

+ (void)getAllClusters
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if([WMDBLayer clustersCount] == 0) {
            CLLocationCoordinate2D topLeft = CLLocationCoordinate2DMake(-86., -180.);
            CLLocationCoordinate2D bottomRight = CLLocationCoordinate2DMake(86., 180.);
            [WMPointsHandler sharedHandler].clusterRequestsBlocked = NO;
            [[WMPointsHandler sharedHandler] getClustersForArea:topLeft bottomRight:bottomRight withDuplicateCheck:NO];
            [WMPointsHandler sharedHandler].clusterRequestsBlocked = YES;
        }
        else {
            [WMPointsHandler sharedHandler].clusterRequestsBlocked = NO;

//            // TMP (fetch all points)
//            [WMPointsHandler sharedHandler].tmpClusters = [NSMutableArray array];
//            for(WMCluster *cluster in [WMDBLayer allClusters]) {
//                if(!cluster.isPointsUpdated.boolValue)
//                    [[WMPointsHandler sharedHandler].tmpClusters addObject:cluster.identifier];
//            }
//            [WMPointsHandler sharedHandler].currentTmpClusterIndex = 0;
//            [[WMPointsHandler sharedHandler] testDownloadAllPoints];
        }
    });
}

- (void)testDownloadAllPoints
{
//    if(self.tmpClusters == nil) {
//        self.tmpClusters = [WMDBLayer allClusters];
//    }
    int currentClusterID = [[self.tmpClusters objectAtIndex:self.currentTmpClusterIndex] intValue];
    ++self.currentTmpClusterIndex;
    [WMPointsHandler getPointsForClusterId:currentClusterID];
}

+ (void)getPointsForArea:(CLLocationCoordinate2D)topLeft bottomRight:(CLLocationCoordinate2D)bottomRight
{
    [[WMPointsHandler sharedHandler] getPointsForArea:topLeft bottomRight:bottomRight withDuplicateCheck:YES];
}

+ (void)getPointsForClusterId:(int)clusterId
{
    WMCluster *cluster = [WMDBLayer clusterWithId:clusterId];
    if(cluster.isPointsUpdated.boolValue) {
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:clusterId] forKey:@"cluster_id"];
        [[NSNotificationCenter defaultCenter] postNotificationName:nDID_UPDATE_POINTS object:self userInfo:userInfo];

//#warning TMP
//        [[self sharedHandler] testDownloadAllPoints];

        return;
    }

    NSString *url = [self urlForPointsOfClusters:clusterId, -1];
    [[WMPointsHandler sharedHandler] getPointsFromUrl:url];
}

+ (void)getPointsForClusters:(NSArray *)clusters
{
//    NSString *url = [self urlForPointsOfClustersArray:clusters];
//    [[WMPointsHandler sharedHandler] getPointsFromUrl:url];
}

+ (void)getAllPoints
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        if([WMDBLayer clustersCount] == 0) {
//        CLLocationCoordinate2D topLeft = CLLocationCoordinate2DMake(-86., -180.);
//        CLLocationCoordinate2D bottomRight = CLLocationCoordinate2DMake(86., 180.);
            CLLocationCoordinate2D topLeft = CLLocationCoordinate2DMake(60.14860376297514, 30.0586886331439);
            CLLocationCoordinate2D bottomRight = CLLocationCoordinate2DMake(59.72900121312431, 30.58606825768947);
            [WMPointsHandler sharedHandler].clusterRequestsBlocked = NO;
            [[WMPointsHandler sharedHandler] getPointsForArea:topLeft bottomRight:bottomRight withDuplicateCheck:YES];
            [WMPointsHandler sharedHandler].clusterRequestsBlocked = YES;
//        }
//        else {
//            [WMPointsHandler sharedHandler].clusterRequestsBlocked = NO;
//        }
    });
}

+ (void)addNewPoint:(CLLocationCoordinate2D)coordinate name:(NSString *)name password:(NSString *)password onFinish:(void (^)(BOOL success))onFinish
{
    NSString *urlStr = [NSString stringWithFormat:URL_FOR_NEW_POINT, coordinate.latitude,
                        coordinate.longitude, @"", @"", @"", @"", @"", @"", name, @"", password, @""];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];

    [NSURLConnection sendAsynchronousRequest:request queue:[WMPointsHandler sharedHandler].downloadQueue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if(connectionError) {
             NSLog(@"Connection error: %@", connectionError.userInfo);
             if(onFinish) {
                 onFinish(NO);
             }
             return;
         }

         NSError *error = nil;
         NSArray *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

         if(error) {
             NSLog(@"JSON data contains error: %@", error.userInfo);
             if(onFinish) {
                 onFinish(NO);
             }
             return;
         }

         if(onFinish) {
             onFinish(YES);
         }

     }];

}



#pragma mark - private

- (NSOperationQueue*)downloadQueue
{
    if(_downloadQueue == nil) {
        _downloadQueue = [[NSOperationQueue alloc] init];
    }
    return _downloadQueue;
}


/**
 Requests clusters for given area and saves in background
 @param topLeft top-left coordinate of requested area
 @param bottomRight bottom-right coordinate of requested area
 @param checkForDuplicates defines if the database should be checked for duplicate clusters.
 @b Warning: setting this parameter to YES with large area may take long time to save all clusters
 */
- (void)getClustersForArea:(CLLocationCoordinate2D)topLeft bottomRight:(CLLocationCoordinate2D)bottomRight withDuplicateCheck:(BOOL)checkForDuplicates
{
    if(self.clusterRequestsBlocked) {
        return;
    }

    NSString *urlStr = [NSString stringWithFormat:URL_FOR_CLUSTERS,
                        topLeft.latitude, topLeft.longitude,
                        bottomRight.latitude, bottomRight.longitude];

    // TEST
//    NSString *urlStr = [NSString stringWithFormat:@"http://5.175.192.184/?cluster=%d", 985];


    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];

    [NSURLConnection sendAsynchronousRequest:request queue:self.downloadQueue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if(connectionError) {
             NSLog(@"Connection error: %@", connectionError.userInfo);
             return;
         }

         NSError *error = nil;
         NSArray *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

         if(error) {
             NSLog(@"JSON data contains error: %@", error.userInfo);
             return;
         }
         
         NSTimeInterval time = [NSDate timeIntervalSinceReferenceDate];

         if(checkForDuplicates) {
             for(NSDictionary *cluster in jsonResult) {
                 int16_t ID = [[cluster objectForKey:@"id"] intValue];
                 double lat = [[cluster objectForKey:@"lat"] doubleValue];
                 double lon = [[cluster objectForKey:@"lon"] doubleValue];
                 NSDate *createDate = [GUtils dateFromString:[cluster objectForKey:@"inserted"]];
                 NSDate *updateDate = [GUtils dateFromString:[cluster objectForKey:@"updated"]];
                 NSNumber *x = [GUtils decimalFromString:[cluster objectForKey:@"x"]];
                 NSNumber *y = [GUtils decimalFromString:[cluster objectForKey:@"y"]];
                 
                 WMCluster *cluster = [WMDBLayer createClusterWithId:ID latitude:lat longitude:lon];
                 if(cluster) {
                     cluster.createDate = createDate;
                     cluster.updateDate = updateDate;
                     cluster.x = x;
                     cluster.y = y;
                 }
             }
         }
         else {
             for(NSDictionary *clusterObject in jsonResult) {
                 int16_t ID = [[clusterObject objectForKey:@"id"] intValue];
                 double lat = [[clusterObject objectForKey:@"lat"] doubleValue];
                 double lon = [[clusterObject objectForKey:@"lng"] doubleValue];
                 NSDate *createDate = [GUtils dateFromString:[clusterObject objectForKey:@"inserted"]];
                 NSDate *updateDate = [GUtils dateFromString:[clusterObject objectForKey:@"updated"]];
                 NSNumber *x = [GUtils decimalFromString:[clusterObject objectForKey:@"x"]];
                 NSNumber *y = [GUtils decimalFromString:[clusterObject objectForKey:@"y"]];
                 
                 WMCluster *cluster = [WMDBLayer createUnsafeClusterWithId:ID latitude:lat longitude:lon];
                 cluster.createDate = createDate;
                 cluster.updateDate = updateDate;
                 cluster.x = x;
                 cluster.y = y;
             }
         }

         self.clusterRequestsBlocked = NO;

         time = [NSDate timeIntervalSinceReferenceDate] - time;
         DebugLog(@"creating %d clusters took %g seconds", jsonResult.count, time);

         time = [NSDate timeIntervalSinceReferenceDate];
         [[WMDBLayer sharedLayer] saveContext];

         time = [NSDate timeIntervalSinceReferenceDate] - time;
         DebugLog(@"saving DB took %g seconds", time);

         [[NSNotificationCenter defaultCenter] postNotificationName:nDID_UPDATE_CLUSTERS object:self];
     }];
}

/**
 Requests points for given area and saves in background
 @param topLeft top-left coordinate of requested area
 @param bottomRight bottom-right coordinate of requested area
 @param checkForDuplicates defines if the database should be checked for duplicate clusters.
 @b Warning: setting this parameter to YES with large area may take long time to save all points
 */
- (void)getPointsForArea:(CLLocationCoordinate2D)topLeft bottomRight:(CLLocationCoordinate2D)bottomRight withDuplicateCheck:(BOOL)checkForDuplicates
{
    if(self.clusterRequestsBlocked) {
        return;
    }

    double lat1 = fmin(topLeft.latitude, bottomRight.latitude);
    double lat2 = fmax(topLeft.latitude, bottomRight.latitude);
    double lon1 = fmin(topLeft.longitude, bottomRight.longitude);
    double lon2 = fmax(topLeft.longitude, bottomRight.longitude);
    
    NSString *urlStr = [NSString stringWithFormat:URL_FOR_POINTS, lat1, lon1, lat2, lon2];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];

    NSTimeInterval requestTime = [NSDate timeIntervalSinceReferenceDate];
    
    [NSURLConnection sendAsynchronousRequest:request queue:self.downloadQueue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if(connectionError) {
             NSLog(@"Connection error: %@", connectionError.userInfo);
             return;
         }
         
         NSError *error = nil;
         NSArray *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
         
         if(error) {
             NSLog(@"JSON data contains error: %@", error.userInfo);
             return;
         }

         DebugLog(@"response time: %g", [NSDate timeIntervalSinceReferenceDate] - requestTime);
         
         NSTimeInterval time = [NSDate timeIntervalSinceReferenceDate];
         
         for(NSDictionary *pointObject in jsonResult) {
             int16_t clusterID = [[pointObject objectForKey:@"cluster"] intValue];
             NSString *ID = [pointObject objectForKey:@"id"];
             NSNumber *lat = [GUtils decimalFromString:[pointObject objectForKey:@"lat"]];
             NSNumber *lon = [GUtils decimalFromString:[pointObject objectForKey:@"lng"]];
             
             WMPoint *point;
             if(checkForDuplicates) {
                 point = [WMDBLayer createPointWithId:ID inClusterID:clusterID latitude:lat longitude:lon];
             }
             else {
                 point = [WMDBLayer createUnsafePointWithId:ID inClusterID:clusterID latitude:lat longitude:lon];
             }

             if(point) {
                 point.createDate = [GUtils dateFromString:[pointObject objectForKey:@"inserted"]];
                 point.updateDate = [GUtils dateFromString:[pointObject objectForKey:@"updated"]];
                 point.x = [GUtils decimalFromString:[pointObject objectForKey:@"x"]];
                 point.y = [GUtils decimalFromString:[pointObject objectForKey:@"y"]];
                 
                 point.country = [pointObject objectForKey:@"country"];
                 point.state = [pointObject objectForKey:@"state"];
                 point.city = [pointObject objectForKey:@"city"];
                 point.address = [pointObject objectForKey:@"address"];
                 point.street = [pointObject objectForKey:@"street"];
                 point.kind = [pointObject objectForKey:@"kind"];
                 point.title = [pointObject objectForKey:@"title"];
                 point.isOpen = [NSNumber numberWithBool:[[pointObject objectForKey:@"isOpen"] boolValue]];
                 point.status = [pointObject objectForKey:@"status"];
                 point.comment = [[pointObject objectForKey:@"comment"] componentsJoinedByString:@"\n\n"];
             }
         }
     
         self.clusterRequestsBlocked = NO;
     
         time = [NSDate timeIntervalSinceReferenceDate] - time;
         DebugLog(@"creating %d points with DUPLICATE CHECK took %g seconds", jsonResult.count, time);
         
         time = [NSDate timeIntervalSinceReferenceDate];
         [[WMDBLayer sharedLayer] saveContext];
         
         time = [NSDate timeIntervalSinceReferenceDate] - time;
         DebugLog(@"saving DB took %g seconds", time);
         
         [[NSNotificationCenter defaultCenter] postNotificationName:nDID_UPDATE_POINTS object:self];
     }];
}


- (void)getPointsFromUrl:(NSString*)url
{
    if(url == nil || url.length == 0) {
        return;
    }

    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:self.downloadQueue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if(connectionError) {
             NSLog(@"Connection error: %@", connectionError.userInfo);
             return;
         }

         NSError *error = nil;
         NSArray *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

         if(error) {
             NSLog(@"JSON data contains error: %@", error.userInfo);

//#warning TMP
//             [self testDownloadAllPoints];

             return;
         }

         if(jsonResult.count == 0) {
             DebugLog(@"no points in requested cluster");

//#warning TMP
//             [self testDownloadAllPoints];

             return;
         }

         NSTimeInterval time = [NSDate timeIntervalSinceReferenceDate];

         int clusterId = [[[jsonResult objectAtIndex:0] objectForKey:@"cluster"] integerValue];
         WMCluster *cluster = [WMDBLayer clusterWithId:clusterId];
         if(cluster == nil) {
             DebugLog(@"cluster with id %d not found in db", clusterId);
             
//#warning TMP
//             [self testDownloadAllPoints];
             
             return;
         }


         for(NSDictionary *pointObject in jsonResult) {
             NSString *identifier = [pointObject objectForKey:@"id"];
             NSNumber *lat = [NSNumber numberWithDouble:[[pointObject objectForKey:@"lat"] doubleValue]];
             NSNumber *lon = [NSNumber numberWithDouble:[[pointObject objectForKey:@"lng"] doubleValue]];

             WMPoint *point = [WMDBLayer createUnsafePointWithId:identifier inCluster:cluster latitude:lat longitude:lon];
             if(point == nil) {
                 continue;
             }

//             point.createDate = [pointObject objectForKey:@"inserted"];
//             point.updateDate = [pointObject objectForKey:@"updated"];
             point.x = [NSNumber numberWithDouble:[[pointObject objectForKey:@"x"] doubleValue]];
             point.y = [NSNumber numberWithDouble:[[pointObject objectForKey:@"y"] doubleValue]];
             point.country = [pointObject objectForKey:@"country"];
             point.state = [pointObject objectForKey:@"state"];
             point.city = [pointObject objectForKey:@"city"];
             point.address = [pointObject objectForKey:@"address"];
             point.street = [pointObject objectForKey:@"street"];
             point.kind = [pointObject objectForKey:@"kind"];
             point.title = [pointObject objectForKey:@"title"];
             point.isOpen = [NSNumber numberWithBool:[[pointObject objectForKey:@"isOpen"] boolValue]];
             point.status = [pointObject objectForKey:@"status"];

//             NSString *comments = [[[pointObject objectForKey:@"comment"] valueForKey:@"description"] componentsJoinedByString:@"\n\n"];
             point.comment = [[pointObject objectForKey:@"comment"] componentsJoinedByString:@"\n\n"];

//             [cluster addPointsObject:point];
         }

         cluster.isPointsUpdated = [NSNumber numberWithBool:YES];

         [[WMDBLayer sharedLayer] saveContext];

         time = [NSDate timeIntervalSinceReferenceDate] - time;
         DebugLog(@"saving %d points took %g seconds", jsonResult.count, time);

         
//#warning TMP
//         [self testDownloadAllPoints];
         
         NSDictionary *userInfo = [NSDictionary dictionaryWithObject:cluster.identifier forKey:@"cluster_id"];
         [[NSNotificationCenter defaultCenter] postNotificationName:nDID_UPDATE_POINTS object:self userInfo:userInfo];
     }];
}



/**
 Generates and returns url string for requesting points for clusters
 @param cluster1,... A comma separated list of cluster IDs ending with negative value
 @return String of url for sending request
 */
+ (NSString*)urlForPointsOfClusters:(int)cluster1, ...
{
    va_list args;
    va_start(args, cluster1);
    int arg = cluster1;
    NSMutableString *urlStr = [NSMutableString stringWithFormat:@"%@%d", URL_FOR_CLUSTER_POINTS, arg];

    while (true) {
        arg = va_arg(args, int);
        if(arg < 0) break;
        [urlStr appendFormat:@",%d", arg];
    }
    va_end(args);

    return urlStr;
}

+ (NSString*)urlForPointsOfClustersArray:(NSArray*)clusters
{
    if(clusters == nil || clusters.count == 0) {
        return nil;
    }

    WMCluster *cluster = [clusters objectAtIndex:0];
    NSMutableString *urlStr = [NSMutableString stringWithFormat:@"%@%d", URL_FOR_CLUSTER_POINTS, [cluster.identifier intValue]];
    for(int i = 1; i < clusters.count; ++i) {
        cluster = [clusters objectAtIndex:i];
        [urlStr appendFormat:@",%d", [cluster.identifier intValue]];
    }

    return urlStr;
}


@end















