//
//  WMPointDetailsViewController.h
//  WifiMap
//
//  Created by Gor on 15/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WMPoint;

@protocol WMPointDetailsDelegate <NSObject>

@optional
- (void)nearbyButtonTapped;

@end

@interface WMPointDetailsViewController : UIViewController

+ (instancetype)sharedController;

@property (nonatomic, weak) WMPoint *point;
@property (nonatomic, weak) id<WMPointDetailsDelegate> delegate;

@end
