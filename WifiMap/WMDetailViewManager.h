

#import <UIKit/UIKit.h>

/*
 SubstitutableDetailViewController defines the protocol that detail view controllers must adopt.
 The protocol specifies aproperty for the bar button item controlling the navigation pane.
 */
@protocol SubstitutableDetailViewController

@optional
@property (nonatomic, strong) UIBarButtonItem *navigationPaneBarButtonItem;

@end

@interface WMDetailViewManager : NSObject <UISplitViewControllerDelegate>

/// Things for IB
// The split view this class will be managing.
@property (nonatomic, strong) IBOutlet UISplitViewController *splitViewController;
@property (weak, nonatomic) IBOutlet UITableViewController *tableViewController;
@property (weak, nonatomic) IBOutlet UINavigationController *navController;

// The presently displayed detail view controller.  This is modified by the various 
// view controllers in the navigation pane of the split view controller.
@property (nonatomic, assign) IBOutlet UIViewController<SubstitutableDetailViewController> *detailViewController;

@end
