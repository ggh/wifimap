//
//  WMNearbyViewController.h
//  WifiMap
//
//  Created by Gor on 16/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface WMNearbyViewController : UIViewController

+ (instancetype)sharedController;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) CLLocationCoordinate2D userCoord;
@property (nonatomic, assign) CLLocationDistance maxDistance;
@property (nonatomic, assign) CLLocationDistance maxVisibleDistance;

@end
