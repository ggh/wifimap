//
//  EPUtils.m
//  ElaraMobile
//
//  Created by Gor on 12/8/12.
//  Copyright (c) 2012 G. All rights reserved.
//

#import "GUtils.h"
#import <SystemConfiguration/CaptiveNetwork.h>

@implementation NSString (GUtils)

- (BOOL)containsString:(NSString *)string
{
    NSRange range = [self rangeOfString:string];
    if(range.length > 0) {
        return YES;
    }
    return NO;
}

@end



@interface GUtils ()

@property (nonatomic, strong) Reachability *reachability;
@property (nonatomic, strong) NSArray *mapKeys;

@end

@implementation GUtils

@synthesize didRateGood = _didRateGood;
@synthesize currentWifiName = _currentWifiName;

+ (GUtils*)sharedUtils
{
    static GUtils *utils;
    if(nil != utils) {
        return utils;
    }

    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        utils = [[GUtils alloc] init];
    });
    return utils;
}

- (id)init
{
    self = [super init];
    if(self) {
        [self initMapKeys];
    }
    return self;
}

- (BOOL)didRateGood
{
    _didRateGood = [[NSUserDefaults standardUserDefaults] boolForKey:@"did_rate_good"];
    return _didRateGood;
}

- (void)setDidRateGood:(BOOL)didRateGood
{
    [[NSUserDefaults standardUserDefaults] setBool:didRateGood forKey:@"did_rate_good"];
    _didRateGood = didRateGood;
}

+ (BOOL)isFirstRun
{
    static NSInteger isFirst = -1;
    if(isFirst == -1) {
        isFirst = [[[NSUserDefaults standardUserDefaults] objectForKey:@"first_run"] integerValue];
        if(isFirst != 100) {
            [[NSUserDefaults standardUserDefaults] setInteger:100 forKey:@"first_run"];
            return YES;
        }
    }
    return NO;
}

+ (void)enableReachabilityCheck:(BOOL)enable
{
    [self enableReachabilityCheck:enable wifiOnly:NO];
}

+ (void)enableReachabilityCheck:(BOOL)enable wifiOnly:(BOOL)wifiOnly
{
    Reachability *reachability = [self sharedUtils].reachability;
    if(!enable) {
        if(reachability) {
            [reachability stopNotifier];
        }
    }
    else {
        if(!reachability) {
            reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
        }
        reachability.reachableOnWWAN = !wifiOnly;
        [reachability startNotifier];
        [self sharedUtils].reachability = reachability;
    }
}


+ (UIColor*)colorFromString:(NSString *)colorStr
{
    NSScanner *scanner = [NSScanner scannerWithString:colorStr];

    // it seems the scipping doesn't work properly, so it's better to keep color string in format FFFFFF
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]];

    uint rgb;
    [scanner scanHexInt:&rgb];
    CGFloat r = ((rgb & 0xFF0000) >> 16) / 255.0f;
    CGFloat g = ((rgb & 0x00FF00) >>  8) / 255.0f;
    CGFloat b = (rgb & 0x0000FF) / 255.0f;

    UIColor *color = [UIColor colorWithRed:r green:g blue:b alpha:1.0f];
    return color;
}

+ (NSString*)formatedDateFromDate:(NSDate*)date withTime:(BOOL)withTime
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    if(withTime) {
        [f setTimeStyle:NSDateFormatterShortStyle];
    }
    else {
        [f setTimeStyle:NSDateFormatterNoStyle];
    }
    [f setDateStyle:NSDateFormatterMediumStyle];
    NSString *s = [f stringFromDate:date];
    return s;
}

+ (NSString*)formatedTimeFromDate:(NSDate *)date
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setTimeStyle:NSDateFormatterShortStyle];
    [f setDateStyle:NSDateFormatterNoStyle];
    NSString *s = [f stringFromDate:date];
    return s;
}

+ (NSDate*)dateFromString:(NSString *)dateStr
{
    NSString *dStr = [dateStr stringByReplacingOccurrencesOfString:@"000Z" withString:@""];
    dStr = [dStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dStr = [dStr stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    NSLog(@"dateStr: %@\ndStr: %@", dateStr, dStr);

    static NSDateFormatter *df = nil;
    if(df == nil) {
        df = [[NSDateFormatter alloc] init];
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [df setTimeZone:gmt];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }

    NSDate *date = [df dateFromString:dStr];
    return date;
}

+ (NSNumber*)decimalFromString:(NSString *)numberStr
{
    static NSNumberFormatter *numberFormatter = nil;
    if(numberFormatter == nil) {
        numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    }

    return [numberFormatter numberFromString:numberStr];
}

+ (BOOL)isToday:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(/*NSEraCalendarUnit|*/NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(/*NSEraCalendarUnit|*/NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:date];
    NSDate *otherDate = [cal dateFromComponents:components];

    if([today isEqualToDate:otherDate]) {
        return YES;
    }

    return NO;
}

+ (BOOL)isScreenRetina
{
    static BOOL firstCall = YES;
    static BOOL isRetina;
    if(!firstCall) {
        // avoid later calls of the expansive part
        return isRetina;
    }
    firstCall = NO;

    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        isRetina = YES;
    }
    else {
        isRetina = NO;
    }
    return isRetina;
}

+ (BOOL)isScreenRetina4
{
    static BOOL firstCall = YES;
    static BOOL isRetina4;
    if(!firstCall) {
        // avoid later calls of the expansive part
        return isRetina4;
    }
    firstCall = NO;
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f) {
        isRetina4 = YES;
    }
    else {
        isRetina4 = NO;
    }
    return isRetina4;
}

+ (float)downloadZoomMin
{
    static float zoom = 0;
    if(zoom == 0) {
        zoom = fabsf([[[NSBundle mainBundle] objectForInfoDictionaryKey:@"DownloadZoomMin"] floatValue]);
    }
    return zoom;
}

+ (float)downloadZoomMax
{
    static float zoom = 0;
    if(zoom == 0) {
        zoom = fabsf([[[NSBundle mainBundle] objectForInfoDictionaryKey:@"DownloadZoomMax"] floatValue]);
    }
    return zoom;
}

+ (NSString*)mapBoxKey
{
    NSString *key;
    NSInteger today = [[[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:[NSDate date]] day] - 1;
    key = [[self sharedUtils].mapKeys objectAtIndex:today];
    return key;
}

- (void)checkWifiStatus
{
    NSArray *ifs = (id)CFBridgingRelease(CNCopySupportedInterfaces());
    NSLog(@"%s: Supported interfaces: %@", __func__, ifs);
    id info = nil;
    for (NSString *ifnam in ifs) {
        info = (__bridge id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        NSLog(@"%s: %@ => %@", __func__, ifnam, info);
        if (info && [info count]) {
            break;
        }
    }

    NSString *networkName = nil;
    if(info) {
        networkName = [info objectForKey:@"SSID"];
        DebugLog(@"network name: %@", networkName);
    }

    BOOL shouldNotify = NO;
//    if((networkName != nil && _currentWifiName == nil) || (networkName == nil && _currentWifiName != nil) ||
    if(networkName != _currentWifiName) {
        shouldNotify = YES;
    }
    _currentWifiName = networkName;

    if(shouldNotify) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kWIFI_STATE_UPDATED object:nil];
    }
}


/*
 DISCLAIMER
 
 FOLLOWING FUNCTIONALITY IS IMPLEMENTED FOR TESTING PURPOSES ONLY!
 DEVELOPER IS NOT RESPONSIBLE FOR ANY VIOLATION OF MAPBOX TERMS OF USE OR LICENSE AGREEMENT
 IF FOLLOWING (OR ANY CONNECTED) FUNCTIONALITY IS USED IN PRODUCTION VERSION OF APP FOR ANY
 OTHER PURPOSES, WHICH CAN BE SUBJECT OF VIOLATION OF ANY LEGAL TERMS
 */
- (void)initMapKeys
{
    // ORIGINAL
//    self.mapKeys = [NSArray arrayWithObjects:
//                    @"black99.hk1837pj",
//                    @"mannax.hk18a2mf",
//                    @"bablorub.hk18fc70",
//                    @"amantac.hk18kgcd",
//                    @"fds89hgt.hk18om8j",
//                    @"svenskamap.hk194h9f",
//                    @"primefactor.hk1a20k0",
//                    @"drno.hk1a6chm",
//                    @"oracularspectacular.hk1aa68i",
//                    @"goddamn.hk1afn9c",
//                    @"superpowersonline.hk1ajcib",
//                    @"isleofdog34.hk1ao2mp",
//                    @"whynotgirl78.hk1b2a28",
//                    @"blackfriday.hk1ba2m6",
//                    @"blackieinthebox.hk1bdgcl",
//                    @"dungeonkeepr.hk1bi6mm",
//                    @"bibliorub.hk1blp7l",
//                    @"digiol.hlgdljbk",
//                    @"spedomtrtf.hlge2og1",
//                    @"heroldk.hlge7hjp",
//                    @"bloggersprm.hlgf3b10",
//                    @"altrusspencer.hlgfbjf4",
//                    @"spokespersonm.hlgfgh63",
//                    @"bermuda23.hlgfmmdl",
//                    @"whynotwhy2.hlgg2pgn",
//                    @"bengaza435.hlgg8j7b",
//                    @"sepman23.hlgggbje",
//                    @"ibragim345.hlggj87e",
//                    @"sxdoll3245.hlggp8eb",
//                    @"seniorman2.hlgh3fkf",
//                    nil];

    self.mapKeys = [NSArray arrayWithObjects:
                    @"fsdfas.i9ne2bo1",
                    @"ferg3425.i9nfk3bk",
                    @"nbvn5654bcv.i9ng42b7",
                    @"vnxmcv234.i9ngih8e",
                    @"fdvbdfggh4.i9nj45k3",
                    @"nmbvmv.i9nk09k5",
                    @"zxcvzvzx.i9nkg46n",
                    @"fsadfasdf.i9nlcef1",
                    @"fasdfa.i9nmb0bg",
                    @"fasdfzcv.i9nocjmb",
                    @"fasdfasdf.i9pkkmb8",
                    @"fasdfasd.i9p809e3",
                    @"fsajdf234safd.ia298l84",
                    @"423r4asf.ia2baa8h",
                    @"fshdf234sf.ia2e9l07",
                    @"sergeydolgoeff.ia2kge1j",
                    @"usmanobashir.ia2l0c5i",
                    @"ibigdan.ia2lkkp7",
                    @"emeljanoffmaksim.ia2mi9c8",
                    @"usmvanovserge.ia2oc8m0",
                    @"fsdafasd.ia2pjom4",
                    @"serezhenkaemelyanov.ia30d3o9",
                    @"generaldegaulle.ia3142l4",
                    @"egorulikov.ia32e8h4",
                    @"pugachevv.ia335542",
                    @"fhitrik.ia341chp",
                    @"alexeybestuzhev.ia34pbng",
                    @"ilijm.ia357eo0",
                    @"toshay.ia35hi9i",
                    @"regard23.ia36414h",
                    nil];

}

+ (NSString*)jsonForKey:(NSString*)key
{
    NSString *json = [NSString stringWithFormat:@"{\"attribution\":\"<a href='https://www.mapbox.com/about/maps/' target='_blank'>&copy; Mapbox &copy; OpenStreetMap</a> <a class='mapbox-improve-map' href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a>\",\"autoscale\":true,\"bounds\":[-180,-85,180,85],\"center\":[30.26420021057129,59.89440155029297,4],\"data\":[\"http://a.tiles.mapbox.com/v3/%@/markers.geojsonp\"],\"description\":\"\",\"geocoder\":\"http://a.tiles.mapbox.com/v3/%@/geocode/{query}.jsonp\",\"id\":\"%@\",\"maxzoom\":19,\"minzoom\":0,\"name\":\"My First Map\",\"private\":true,\"scheme\":\"xyz\",\"tilejson\":\"2.0.0\",\"tiles\":[\"http://a.tiles.mapbox.com/v3/%@/{z}/{x}/{y}.png\",\"http://b.tiles.mapbox.com/v3/%@/{z}/{x}/{y}.png\"],\"webpage\":\"http://a.tiles.mapbox.com/v3/%@/page.html\"}", key, key, key, key, key, key];
//    {"attribution":"<a href='https://www.mapbox.com/about/maps/' target='_blank'>&copy; Mapbox &copy; OpenStreetMap</a> <a class='mapbox-improve-map' href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a>","autoscale":true,"bounds":[-180,-85,180,85],"center":[9,51,4],"data":["http://a.tiles.mapbox.com/v3/altrusspencer.hlgfbjf4/markers.geojsonp"],"description":"","geocoder":"http://a.tiles.mapbox.com/v3/altrusspencer.hlgfbjf4/geocode/{query}.jsonp","id":"altrusspencer.hlgfbjf4","maxzoom":19,"minzoom":0,"name":"My First Map","private":true,"scheme":"xyz","tilejson":"2.0.0","tiles":["http://a.tiles.mapbox.com/v3/altrusspencer.hlgfbjf4/{z}/{x}/{y}.png","http://b.tiles.mapbox.com/v3/altrusspencer.hlgfbjf4/{z}/{x}/{y}.png"],"webpage":"http://a.tiles.mapbox.com/v3/altrusspencer.hlgfbjf4/page.html"}
    return json;
}

@end


















