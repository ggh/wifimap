//
//  EPUtils.h
//  ElaraMobile
//
//  Created by Gor on 12/8/12.
//  Copyright (c) 2012 G. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

//#define LITE_VERSION

#define kGOOGLE_API_KEY     @"AIzaSyCnrA2vgvyGDkFTw3LQtcLCURSn1hWOUdo"
#define kPLACES_API_KEY     @"AIzaSyC-DexVubk2ao6xKLUYr8wtrE2QsdU46Hs"
#define kMAPBOX_API_KEY     [GUtils mapBoxKey]
#define kPRO_URL            @"itms://itunes.apple.com/us/app/paris-paris-guide/"
#define kLITE_URL           @"itms://itunes.apple.com/us/app/paris-paris-guide/"

#define kBLOCK_ORIENTATION_CHANGE       @"block_rotation"
#define kUNBLOCK_ORIENTATION_CHANGE     @"unblock_rotation"
#define kWIFI_STATE_UPDATED             @"wifi_state_updated"

#if !LITE_VERSION
#define kDOWNLOAD_ZOOM_MIN  [GUtils downloadZoomMin]
#define kDOWNLOAD_ZOOM_MAX  [GUtils downloadZoomMax]
#endif

#pragma mark - Debug options

#define ON  1
#define OFF 0

#define TESTING_MODE        ON
#define DISPLAY_DEBUG_VIEWS OFF

#define DEBUG_MODE          ON
#define LOG_DETAIL_LEVEL    0

#define DebugLog(fmt, ...) \
if(DEBUG_MODE == ON) { \
    if(LOG_DETAIL_LEVEL == 2) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__); \
    else if(LOG_DETAIL_LEVEL == 1) NSLog((@"%s " fmt), __PRETTY_FUNCTION__, ##__VA_ARGS__); \
    else if(LOG_DETAIL_LEVEL == 0) NSLog(fmt, ##__VA_ARGS__); \
}


#define IS_RETINA_SCREEN [GUtils isScreenRetina]
#define IS_RETINA4_SCREEN [GUtils isScreenRetina4]
#define IS_IPAD [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad
#define DELTA_HEIGHT 88.f

#define IS_IOS_VERSION(v)                   ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define IS_IOS_VERSION_GREATER(v)           ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define IS_IOS_VERSION_GREATER_OR_EQUAL(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define IS_IOS_VERSION_LESS(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define IS_IOS_VERSION_LESS_OR_EQUAL(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define IS_IOS7                             ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending)



@interface GUtils : NSObject
{
}

@property (nonatomic) BOOL didRateGood;
@property (readonly, nonatomic) NSString *currentWifiName;

+ (GUtils*)sharedUtils;

+ (BOOL)isFirstRun;
+ (void)enableReachabilityCheck:(BOOL)enable;
+ (void)enableReachabilityCheck:(BOOL)enable wifiOnly:(BOOL)wifiOnly;
+ (UIColor*)colorFromString:(NSString*)colorStr;
+ (NSString*)formatedDateFromDate:(NSDate*)date withTime:(BOOL)withTime;
+ (NSString*)formatedTimeFromDate:(NSDate*)date;
+ (NSDate*)dateFromString:(NSString*)dateStr;
+ (NSNumber*)decimalFromString:(NSString*)numberStr;
+ (BOOL)isToday:(NSDate*)date;
+ (BOOL)isScreenRetina;
+ (BOOL)isScreenRetina4;

+ (NSString*)mapBoxKey;
+ (float)downloadZoomMin;
+ (float)downloadZoomMax;

+ (NSString*)jsonForKey:(NSString*)key;

- (void)checkWifiStatus;

@end



















