//
//  GMSMarker+Clustering.m
//  WifiMap
//
//  Created by Gor on 25/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#if LITE_VERSION

#import "GMSMarker+Clustering.h"
#import "ClusterRegion.h"
#import <objc/runtime.h>

static const char * const clusterTag = "cluster_reg";
static const char * const isKeyTag = "key_tag";
static const char * const isGroupTag = "group_tag";


@implementation GMSMarker(Clustering)

- (void)setClusterRegion:(ClusterRegion *)clusterRegion
{
    objc_setAssociatedObject(self, clusterTag, clusterRegion, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (ClusterRegion*)clusterRegion
{
    ClusterRegion* reg = (ClusterRegion*)objc_getAssociatedObject(self, clusterTag);
    return reg;
}


- (void)setIsKey:(BOOL)isKey
{
    objc_setAssociatedObject(self, isKeyTag, [NSNumber numberWithBool:isKey], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)isKey
{
    return [objc_getAssociatedObject(self, isKeyTag) boolValue];
}

- (void)setIsGrouped:(BOOL)isGrouped
{
    objc_setAssociatedObject(self, isGroupTag, [NSNumber numberWithBool:isGrouped], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)isGrouped
{
    return [objc_getAssociatedObject(self, isGroupTag) boolValue];
}



@end


#endif