//
//  WMSplitViewController.m
//  WifiMap
//
//  Created by Gor on 09/04/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import "WMSplitViewController.h"

@interface WMSplitViewController ()

@property (nonatomic) BOOL shouldBlockRotations;

@end

@implementation WMSplitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.shouldBlockRotations = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
#if LITE_VERSION
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(blockOrientationChange)
                                                 name:kBLOCK_ORIENTATION_CHANGE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unblockOrientationChange)
                                                 name:kUNBLOCK_ORIENTATION_CHANGE object:nil];
#endif
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations
{
    if(self.shouldBlockRotations) {
        return UIInterfaceOrientationMaskPortrait;
//        return (UIInterfaceOrientationPortrait | UIInterfaceOrientationPortraitUpsideDown);
    }
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)blockOrientationChange
{
    self.shouldBlockRotations = YES;
    [UIViewController attemptRotationToDeviceOrientation];
}

- (void)unblockOrientationChange
{
    self.shouldBlockRotations = NO;
    [UIViewController attemptRotationToDeviceOrientation];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
