//
//  ClusterRegion.m
//  WifiMap
//
//  Created by Gor on 25/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import "ClusterRegion.h"

#if LITE_VERSION

#import <GoogleMaps/GMSMarker.h>

@implementation ClusterRegion

@synthesize groupMarker = _groupMarker;

+ (instancetype)regionWithRect:(CGRect)rect
{
    ClusterRegion *region = [[ClusterRegion alloc] init];
    region.area = rect;
    region.objectId = -1;
    return region;
}

- (BOOL)involvesScreenPoint:(CGPoint)point
{
    if(CGRectContainsPoint(self.area, point)) {
        return YES;
    }
    return NO;
}

- (void)displayDebugData
{
    if(DISPLAY_DEBUG_VIEWS) {
        dispatch_async(dispatch_get_main_queue(), ^{
            GMSMarker *marker = self.groupMarker;
            BOOL hasGroupMarker = (marker != nil);
            NSString *groupMarker = hasGroupMarker ? @"1" : @"";
            if(self.area.origin.x == 0 && self.area.origin.y == 0) {
                
            }
            NSString *visible = (hasGroupMarker && marker.map != nil) ? @"visible" : @"invisible";
            self.label.text = [NSString stringWithFormat:@"%d /%@ -%@", self.markers.count, groupMarker, visible];

            UIColor *labelColor = [UIColor clearColor];
            if(self.markers.count == 0) {
                self.bgView.backgroundColor = [UIColor colorWithRed:arc4random() % 256 / 255.f
                                                              green:arc4random() % 256 / 255.f
                                                               blue:arc4random() % 256 / 255.f
                                                              alpha:0.2];
                if(hasGroupMarker) {
                    labelColor = [UIColor redColor];
                }
            }
            else {
                self.bgView.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.5];
                if((self.groupMarker && self.groupMarker.map == nil)) {
                    labelColor = [UIColor redColor];
                }
                else {
                    labelColor = [UIColor greenColor];
                }
            }

            self.label.backgroundColor = labelColor;

            [self.label sizeToFit];
        });
    }
}

- (void)addMarker:(GMSMarker *)marker
{
    if(!_markers) {
        _markers = [NSMutableArray array];
    }

    ClusterRegion *currentMarkerRegion = marker.clusterRegion;
    if(currentMarkerRegion == self) {
        return;
    }
    else if(currentMarkerRegion != nil) {
        [currentMarkerRegion removeMarker:marker];
    }

    // if the given marker comes from other region
    // and it was the group (i.e. key) marker of that
    // region, make it key marker of this region
//    if(marker.isKey) {
//        if(self.groupMarker.clusterRegion == self) {
//            self.groupMarker.clusterRegion = nil;
//        }
//        _groupMarker = marker;
//    }

    marker.clusterRegion = self;
    marker.isGrouped = YES;
    @synchronized(@"add_marker") {
        [self.markers addObject:marker];
    }
}

- (void)removeMarker:(GMSMarker *)marker
{
    if(marker.clusterRegion == self) {
        marker.clusterRegion = nil;
        [_markers removeObject:marker];
    }
}

- (void)removeAllMarkers
{
    _markers = nil;
    _markers = [NSMutableArray array];
    _groupMarker = nil;
}

- (GMSMarker*)groupMarker
{
    CGFloat x = self.area.origin.x;
    CGFloat y = self.area.origin.y;
    if(x == 0 && y == 0) {
//        DebugLog(@"stop");
    }
    if(!self.markers || _markers.count == 0) {
        return nil;
    }
    if(_groupMarker/* && _groupMarker.clusterRegion == self*/) {
        return _groupMarker;
    }

    _groupMarker = [self.markers objectAtIndex:0];
    _groupMarker.isKey = YES;
    return _groupMarker;
}

@end


#endif










