//
//  ClusterRegion.h
//  WifiMap
//
//  Created by Gor on 25/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import <Foundation/Foundation.h>
#if LITE_VERSION

#import "GMSMarker+Clustering.h"

@class GMSMarker;

@interface ClusterRegion : NSObject

+ (instancetype)regionWithRect:(CGRect)rect;

- (void)addMarker:(GMSMarker*)marker;
- (void)removeMarker:(GMSMarker*)marker;

/**
 Clears all markers from clustering region
 @warning Marker properties like @b isKey,
 @b isGrouped or @b clusterRegion are not
 cleared by this method. It should be done manually
 */
- (void)removeAllMarkers;

/**
 The area of region in screen rect
 */
@property CGRect area;

/**
 Identifier of object that the region holds.
 
 Defaults to -1: no object being held.
 */
@property int objectId;

@property (nonatomic, strong) NSMutableArray *markers;

@property (nonatomic, strong) GMSMarker *groupMarker;


// For testing only
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *label;
- (void)displayDebugData;



- (BOOL)involvesScreenPoint:(CGPoint)point;


@end

#endif
