//
//  WMDataHandler.m
//  wifiMapTest
//
//  Created by Gor on 11/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//
////
//// DISCLAIMER
//// 
//// FOLLOWING FUNCTIONALITY IS IMPLEMENTED FOR TESTING PURPOSES ONLY!
//// DEVELOPER IS NOT RESPONSIBLE FOR ANY VIOLATION OF MAPBOX TERMS OF USE OR LICENSE AGREEMENT
//// IF FOLLOWING (OR ANY CONNECTED) FUNCTIONALITY IS USED IN PRODUCTION VERSION OF APP FOR ANY
//// OTHER PURPOSES, WHICH CAN BE SUBJECT OF VIOLATION OF ANY LEGAL TERMS
////


#define kTABLE_NAME         @"ZCACHE"
#define kKEY_FIELD          @"cache_key"

#import "WMDataHandler.h"
#import "FMDatabase.h"


@implementation WMDataHandler

+ (WMDataHandler*)sharedLayer
{
    static WMDataHandler *sharedLayer = nil;
    if (nil != sharedLayer) {
        return sharedLayer;
    }
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedLayer = [[WMDataHandler alloc] init];
    });
    return sharedLayer;
}

+ (void)initMapKey
{
    NSString *dbPath = [self savedDBPath];
    if(![[NSFileManager defaultManager] fileExistsAtPath:dbPath]) {
        return;
    }

    NSString *key = [NSString stringWithFormat:@"Mapbox-%@", kMAPBOX_API_KEY];

    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    NSString *updateString = [NSString stringWithFormat:@"UPDATE %@ SET %@ = '%@'", kTABLE_NAME, kKEY_FIELD, key];
    BOOL ok = [database executeUpdate:updateString];
    DebugLog(@"map key %@", ok ? @"set successfully" : @"failed to set!");
    [database close];
}


#pragma mark - private


+ (NSString*)savedDBPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dbPath = [cachePath stringByAppendingPathComponent:@"/RMTileCache.db"];
    if([fileManager fileExistsAtPath:dbPath]) {
        return dbPath;
    }
    return @"";
}


@end










