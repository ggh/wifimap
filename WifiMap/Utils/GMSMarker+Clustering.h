//
//  GMSMarker+Clustering.h
//  WifiMap
//
//  Created by Gor on 25/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//


#if LITE_VERSION

#import <GoogleMaps/GoogleMaps.h>

@class ClusterRegion;

@interface GMSMarker (Clustering)

@property (nonatomic, weak) ClusterRegion *clusterRegion;
@property (nonatomic) BOOL isKey;
@property (nonatomic) BOOL isGrouped;

@end

#endif