//
//  WMInfoWindow.h
//  WifiMap
//
//  Created by Gor on 14/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WMPoint;

@interface WMInfoWindow : UIView

+ (instancetype)infoWindowForPoint:(WMPoint*)point;
+ (instancetype)infoWindowForPoint:(WMPoint*)point disabled:(BOOL)disabled;

@end
