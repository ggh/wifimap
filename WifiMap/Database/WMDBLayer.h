//
//  WMDBLayer.h
//  WifiMap
//
//  Created by Gor on 23/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WMCluster.h"
#import "WMPoint.h"
#import <CoreLocation/CLLocation.h>

@interface WMDBLayer : NSObject

+ (WMDBLayer*)sharedLayer;

+ (void)checkAndCopyDBFile;

/**
 Creates and returns cluster with given identifier and coordinates
 or nil if cluster with identifier already exists
 */
+ (WMCluster*)createClusterWithId:(int16_t)identifier latitude:(double)lat longitude:(double)lon;

/**
 Creates and returns cluster with given identifier and coordinates
 without checking for duplicates
 @warning This method is meant for creating initial clusters table and
 should not be called for creating clusters.
 For that use createClusterWithId:latitude:longitude: method instead
 */
+ (WMCluster*)createUnsafeClusterWithId:(int16_t)identifier latitude:(double)lat longitude:(double)lon;

/**
 Returns cluster with given identifier if it exists in DB and nil, otherwise 
 */
+ (WMCluster*)clusterWithId:(int16_t)identifier;

/**
 Returns saved clusters within given area
 */
+ (NSArray*)clustersInAreaWithTopLeft:(CLLocationCoordinate2D)topLeft andBottomRight:(CLLocationCoordinate2D)bottomRight;


/**
 Returnes saved clusters count
 */
+ (NSUInteger)clustersCount;

/**
 Returnes all saved clusters
 */
+ (NSArray*)allClusters;


/**
 Creates and returns point in given cluster.
 Will return nil if the point with identifier already exists in cluster
 @warning This is relatively slow operation and may take long time if there
 are lots of points to add. For large arrays rather use
 @b createUnsafePointWithId:inCluster:longitude:
 */
+ (WMPoint*)createPointWithId:(NSString*)identifier inCluster:(WMCluster*)cluster latitude:(NSNumber*)lat longitude:(NSNumber*)lon;
+ (WMPoint*)createPointWithId:(NSString*)identifier inClusterID:(int)clusterID latitude:(NSNumber*)lat longitude:(NSNumber*)lon;

/**
 Creates and returns point to the cluster.
 @warning There is no check for duplicates. This method should be used to
 add large arrays of points to the cluster when it's empty.

 This will work much faster than @b createPointWithId
 */
+ (WMPoint*)createUnsafePointWithId:(NSString*)identifier inCluster:(WMCluster*)cluster latitude:(NSNumber*)lat longitude:(NSNumber*)lon;
+ (WMPoint*)createUnsafePointWithId:(NSString*)identifier inClusterID:(int)clusterID latitude:(NSNumber*)lat longitude:(NSNumber*)lon;

+ (NSArray*)pointsForClusterID:(int)clusterID;

+ (NSArray*)pointsForClusterIDs:(NSArray*)clusterIDs;

+ (NSArray*)pointsForArea:(CLLocationCoordinate2D)topLeft bottomRight:(CLLocationCoordinate2D)bottomRight;

// test
+ (NSArray*)allPoints __deprecated_msg("This is for testing only");

- (void)saveContext;

/**
 Returns point with given ID
 */
+ (WMPoint*)pointWithId:(NSString*)identifier;

@end







