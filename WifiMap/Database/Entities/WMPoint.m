//
//  WMPoint.m
//  WifiMap
//
//  Created by Gor on 01/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import "WMPoint.h"
#import "WMCluster.h"


@implementation WMPoint

@dynamic identifier;
@dynamic createDate;
@dynamic updateDate;
@dynamic lat;
@dynamic lon;
@dynamic x;
@dynamic y;
@dynamic country;
@dynamic state;
@dynamic city;
@dynamic address;
@dynamic street;
@dynamic kind;
@dynamic title;
@dynamic comment;
@dynamic status;
@dynamic isOpen;
@dynamic cluster;

@dynamic distance;

@end
