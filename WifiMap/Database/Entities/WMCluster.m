//
//  WMCluster.m
//  WifiMap
//
//  Created by Gor on 01/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import "WMCluster.h"
#import "WMPoint.h"


@implementation WMCluster

@dynamic createDate;
@dynamic identifier;
@dynamic lat;
@dynamic lon;
@dynamic updateDate;
@dynamic x;
@dynamic y;
@dynamic isPointsUpdated;
@dynamic points;

@end
