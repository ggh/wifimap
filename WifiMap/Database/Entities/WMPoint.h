//
//  WMPoint.h
//  WifiMap
//
//  Created by Gor on 01/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class WMCluster;

@interface WMPoint : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSDate * createDate;
@property (nonatomic, retain) NSDate * updateDate;
@property (nonatomic, retain) NSNumber * lat;
@property (nonatomic, retain) NSNumber * lon;
@property (nonatomic, retain) NSNumber * x;
@property (nonatomic, retain) NSNumber * y;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * street;
@property (nonatomic, retain) NSString * kind;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSNumber * isOpen;
@property (nonatomic, retain) WMCluster *cluster;

@property (nonatomic, assign) double distance;

@end
