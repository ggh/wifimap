//
//  WMCluster.h
//  WifiMap
//
//  Created by Gor on 01/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class WMPoint;

@interface WMCluster : NSManagedObject

@property (nonatomic, retain) NSDate * createDate;
@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSNumber * lat;
@property (nonatomic, retain) NSNumber * lon;
@property (nonatomic, retain) NSDate * updateDate;
@property (nonatomic, retain) NSNumber * x;
@property (nonatomic, retain) NSNumber * y;
@property (nonatomic, retain) NSNumber * isPointsUpdated;
@property (nonatomic, retain) NSSet *points;
@end

@interface WMCluster (CoreDataGeneratedAccessors)

- (void)addPointsObject:(WMPoint *)value;
- (void)removePointsObject:(WMPoint *)value;
- (void)addPoints:(NSSet *)values;
- (void)removePoints:(NSSet *)values;

@end
