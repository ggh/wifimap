//
//  NSManagedObjectContext+EM.m
//  ElaraMobile
//
//  Created by Gor on 23/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import "NSManagedObjectContext+WM.h"

@implementation NSManagedObjectContext (EM)


-(NSArray*)fetchObjectsForEntityWithName:(NSString *)entityName sortByKeyOrNil:(NSString *)key
                           withPredicate:(NSString *)predicateText, ...
{
    if(nil == entityName || [entityName isEqualToString:@""]) {
        return nil;
    }
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self];
    [request setIncludesSubentities:NO];
    [request setEntity:entity];
    if(nil != predicateText && ![predicateText isEqualToString:@""]) {
        va_list args;
        va_start(args, predicateText);
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateText arguments:args];
        [request setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *objects = [self executeFetchRequest:request error:&error];
    if(nil != error || 0 == objects.count) {
        // Handle error
        return nil;
    }
    if(nil == key) {
        return objects;
    }
    
    NSSortDescriptor *descriptor =  [NSSortDescriptor sortDescriptorWithKey:key ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:descriptor];
    NSArray *sortedObjects = [objects sortedArrayUsingDescriptors:sortDescriptors];
    
    return sortedObjects;
}

- (NSUInteger)fetchCountForEntityName:(NSString *)name
{
    return [[self fetchObjectsForEntityWithName:name sortByKeyOrNil:nil withPredicate:nil] count];
}

- (NSFetchRequest*)fetchRequestWithEntity:(NSString *)entityName sortedByKey:(NSString *)keyOrNil
                                ascending:(BOOL)ascending batchSize:(NSUInteger)batchSize
{
    if(nil == entityName || [entityName isEqualToString:@""]) {
        return nil;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self];
    [fetchRequest setEntity:entity];

    NSAssert(nil != keyOrNil, @"WARNING: nil value for sort descriptor!!");
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:keyOrNil ascending:ascending];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    [fetchRequest setFetchBatchSize:batchSize];
    
    return fetchRequest;
}



@end
