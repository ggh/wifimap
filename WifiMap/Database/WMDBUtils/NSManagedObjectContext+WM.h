//
//  NSManagedObjectContext+EM.h
//  WifiMaps
//
//  Created by Gor on 23/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (WM)

- (NSArray*)fetchObjectsForEntityWithName:(NSString*)entityName
                           sortByKeyOrNil:(NSString*)key
                            withPredicate:(NSString*)predicateText, ...;

- (NSUInteger)fetchCountForEntityName:(NSString*)name;

- (NSFetchRequest*)fetchRequestWithEntity:(NSString*)entityName
                              sortedByKey:(NSString*)keyOrNil
                                ascending:(BOOL)ascending
                                batchSize:(NSUInteger)batchSize;

@end
