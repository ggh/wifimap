//
//  WMDBLayer.m
//  WifiMap
//
//  Created by Gor on 23/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#define DB_FOLDER_NAME  @"WMPoints"
#define DB_FILE_NAME    @"WMPoints"
#define DB_FILE_TYPE    @"sqlite"


#import "WMDBLayer.h"
#import <CoreData/CoreData.h>
#import "NSManagedObjectContext+WM.h"
//#import "WMCluster.h"
#import "SSZipArchive.h"

@interface WMDBLayer() {
    NSMutableArray *managedObjectContexts;
}

// Core Data
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSPersistentStore *persistentStore;


- (BOOL)saveDB;
+ (NSURL*)applicationDocumentsDirectory;
+ (NSURL*)applicationLibraryDirectory;
+ (NSURL*)applicationCacheDirectory;

@end




@implementation WMDBLayer

@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;
@synthesize persistentStore = _persistentStore;


#pragma mark - public

+ (WMDBLayer*)sharedLayer
{
    static WMDBLayer *sharedLayer = nil;
    if (nil != sharedLayer) {
        return sharedLayer;
    }

    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedLayer = [[WMDBLayer alloc] init];
    });
    return sharedLayer;
}

+ (void)checkAndCopyDBFile
{
    NSString *fileName = [DB_FILE_NAME stringByAppendingPathExtension:DB_FILE_TYPE];
    NSString *targetFolder = [[[self applicationLibraryDirectory] URLByAppendingPathComponent:DB_FOLDER_NAME] path];
    NSString *targetFile = [targetFolder stringByAppendingPathComponent:fileName];
    BOOL isDir = NO;
    BOOL folderExists = [[NSFileManager defaultManager] fileExistsAtPath:targetFolder isDirectory:&isDir];

    NSError *error = nil;
    if(!folderExists || !isDir) {
        BOOL ok = [[NSFileManager defaultManager] createDirectoryAtPath:targetFolder
                                            withIntermediateDirectories:NO
                                                             attributes:nil
                                                                  error:&error];
        if(!ok) {
            DebugLog(@"couldn't create directory %@\nerror: %@", targetFolder, error.userInfo);
            abort();
        }
    }
    if(![[NSFileManager defaultManager] fileExistsAtPath:targetFile isDirectory:NO]) {
//        NSString *mainDBFile = [[NSBundle mainBundle] pathForResource:DB_FILE_NAME ofType:DB_FILE_TYPE];
        NSString *mainDBFile = [[NSBundle mainBundle] pathForResource:DB_FILE_NAME ofType:@"zip"];
//        BOOL ok = [[NSFileManager defaultManager] copyItemAtPath:mainDBFile toPath:targetFile error:&error];
        BOOL ok = [SSZipArchive unzipFileAtPath:mainDBFile toDestination:targetFolder];
        if(!ok) {
//            DebugLog(@"couldn't copy DB from %@ to %@\nerror: %@", mainDBFile, targetFile, error.userInfo);
            DebugLog(@"couldn't unzip DB from %@ to %@", mainDBFile, targetFile);
            abort();
        }

    }

    NSURL *dbFileUrl = [NSURL fileURLWithPath:targetFile];
    BOOL ok = [dbFileUrl setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
    if(!ok) {
        DebugLog(@"error setting db file backup key: %@", error.userInfo);
        abort();
    }
}

+ (WMCluster*)createClusterWithId:(int16_t)identifier latitude:(double)lat longitude:(double)lon
{
    if([WMDBLayer clusterWithId:identifier] != nil) {
        return nil;
    }

    return [self createUnsafeClusterWithId:identifier latitude:lat longitude:lon];
}

+ (WMCluster*)createUnsafeClusterWithId:(int16_t)identifier latitude:(double)lat longitude:(double)lon
{
    NSManagedObjectContext *moc = [self sharedLayer].managedObjectContext;
    WMCluster *cluster = [NSEntityDescription insertNewObjectForEntityForName:@"Cluster" inManagedObjectContext:moc];
    cluster.identifier = [NSNumber numberWithInt:identifier];
    cluster.lat = [NSNumber numberWithDouble:lat];
    cluster.lon = [NSNumber numberWithDouble:lon];
    return cluster;
}

+ (WMCluster*)clusterWithId:(int16_t)identifier
{
    NSManagedObjectContext *moc = [self sharedLayer].managedObjectContext;
    NSArray *clusters = [moc fetchObjectsForEntityWithName:@"Cluster" sortByKeyOrNil:nil withPredicate:@"identifier = %d", identifier];
    if(clusters.count != 1) {
//        NSAssert(clusters.count == 0, @"ERROR: there are more than one clusters with id: %d", identifier);
        return nil;
    }

    return [clusters objectAtIndex:0];
}

+ (NSArray*)clustersInAreaWithTopLeft:(CLLocationCoordinate2D)topLeft andBottomRight:(CLLocationCoordinate2D)bottomRight
{
    CLLocationDegrees lat1 = topLeft.latitude;
    CLLocationDegrees lon1 = topLeft.longitude;
    CLLocationDegrees lat2 = bottomRight.latitude;
    CLLocationDegrees lon2 = bottomRight.longitude;

    NSManagedObjectContext *moc = [self sharedLayer].managedObjectContext;
    NSArray *clusters = [moc fetchObjectsForEntityWithName:@"Cluster" sortByKeyOrNil:nil
                                             withPredicate:@"lat >= %f AND lat <= %f AND lon >= %f AND lon <= %f",
                                                            lat2, lat1, lon1, lon2];
    return clusters;
}

+ (NSUInteger)clustersCount
{
    return [[self sharedLayer].managedObjectContext fetchCountForEntityName:@"Cluster"];
}

+ (NSArray*)allClusters
{
    return [[self sharedLayer].managedObjectContext fetchObjectsForEntityWithName:@"Cluster"
                                                                   sortByKeyOrNil:nil withPredicate:nil];
}

+ (WMPoint*)createPointWithId:(NSString*)identifier inCluster:(WMCluster *)cluster latitude:(NSNumber*)lat longitude:(NSNumber*)lon
{
    if([self pointWithId:identifier] != nil) {
        return nil;
    }

    return [self createUnsafePointWithId:identifier inCluster:cluster latitude:lat longitude:lon];
}

+ (WMPoint*)createPointWithId:(NSString*)identifier inClusterID:(int)clusterID latitude:(NSNumber*)lat longitude:(NSNumber*)lon
{
    if([self pointWithId:identifier] != nil) {
        return nil;
    }
    
    return [self createUnsafePointWithId:identifier inClusterID:clusterID latitude:lat longitude:lon];
}

+ (WMPoint*)createUnsafePointWithId:(NSString *)identifier inCluster:(WMCluster *)cluster latitude:(NSNumber*)lat longitude:(NSNumber*)lon
{
    WMCluster *c = [self clusterWithId:cluster.identifier.intValue];
    if(c.objectID != cluster.objectID) {
        DebugLog(@"Error: clusters mismatch!");
        return nil;
    }

    NSManagedObjectContext *moc = [self sharedLayer].managedObjectContext;
    WMPoint *point = [NSEntityDescription insertNewObjectForEntityForName:@"Point" inManagedObjectContext:moc];
    point.identifier = identifier;
    point.lat = lat;
    point.lon = lon;
    
    [cluster addPointsObject:point];
    
    return point;
}

+ (WMPoint*)createUnsafePointWithId:(NSString *)identifier inClusterID:(int)clusterID latitude:(NSNumber*)lat longitude:(NSNumber*)lon
{
    WMCluster *c = [self clusterWithId:clusterID];
    if(c == nil) {
        DebugLog(@"Cluster not found!");
        return nil;
    }

    NSManagedObjectContext *moc = [self sharedLayer].managedObjectContext;
    WMPoint *point = [NSEntityDescription insertNewObjectForEntityForName:@"Point" inManagedObjectContext:moc];
    point.identifier = identifier;
    point.lat = lat;
    point.lon = lon;

    [c addPointsObject:point];

    return point;
}


+ (NSArray*)pointsForClusterID:(int)clusterID
{
    WMCluster *cluster = [self clusterWithId:clusterID];
    if(cluster == nil) {
        return nil;
    }
    return [cluster.points allObjects];
}

+ (NSArray*)pointsForClusterIDs:(NSArray *)clusterIDs
{
    NSManagedObjectContext *moc = [self sharedLayer].managedObjectContext;
    if(!clusterIDs || clusterIDs.count == 0) {
        return nil;
    }

    int firstID = [[clusterIDs objectAtIndex:0] intValue];
    NSMutableString *predicateStr = [NSMutableString stringWithFormat:@"cluster.identifier = %d", firstID];
    for(int i = 1; i < clusterIDs.count; ++i) {
        [predicateStr appendFormat:@" OR cluster.identifier = %d", [[clusterIDs objectAtIndex:i] intValue]];
    }

    NSArray *points = [moc fetchObjectsForEntityWithName:@"Points" sortByKeyOrNil:nil withPredicate:predicateStr];
    return points;
}


+ (NSArray*)pointsForArea:(CLLocationCoordinate2D)topLeft bottomRight:(CLLocationCoordinate2D)bottomRight
{
    double minLat = fmin(topLeft.latitude, bottomRight.latitude);
    double minLon = fmin(topLeft.longitude, bottomRight.longitude);
    double maxLat = fmax(topLeft.latitude, bottomRight.latitude);
    double maxLon = fmax(topLeft.longitude, bottomRight.longitude);

#if TESTING_MODE
    NSTimeInterval time = [NSDate timeIntervalSinceReferenceDate];
#endif
    NSManagedObjectContext *moc = [self sharedLayer].managedObjectContext;
    NSArray *points = [moc fetchObjectsForEntityWithName:@"Point"
                                          sortByKeyOrNil:nil
                                           withPredicate:@"lat >= %lf AND lat <= %lf AND lon >= %lf AND lon <= %lf",
                                                           minLat, maxLat, minLon, maxLon];
#if TESTING_MODE
    time = [NSDate timeIntervalSinceReferenceDate] - time;
    DebugLog(@"fetching %d points by area took %g seconds", points.count, time);
#endif
    return points;
}

+ (NSArray*)allPoints
{
    return [[self sharedLayer].managedObjectContext fetchObjectsForEntityWithName:@"Point" sortByKeyOrNil:nil withPredicate:nil];
}


#pragma mark - private

- (id)init
{
    self = [super init];
    if(self) {
        managedObjectContexts = [[NSMutableArray alloc] init];
        [self managedObjectContext];
    }
    return self;
}

+ (WMPoint*)pointWithId:(NSString*)identifier
{
    NSTimeInterval time = [NSDate timeIntervalSinceReferenceDate];
    NSManagedObjectContext * moc = [self sharedLayer].managedObjectContext;
    NSArray* fetchedPoints = [moc fetchObjectsForEntityWithName:@"Point"
                                                 sortByKeyOrNil:nil
                                                  withPredicate:@"identifier = %@", identifier];
    if(!fetchedPoints || fetchedPoints.count != 1) {
        if(fetchedPoints.count > 1) {
//            DebugLog(@"found %d points with ID '%@'", fetchedPoints.count, identifier);
        }
        return nil;
    }

    time = [NSDate timeIntervalSinceReferenceDate] - time;
    DebugLog(@"fetching one point took %g seconds", time);

    return [fetchedPoints objectAtIndex:0];
}

- (WMPoint*)pointWithId:(NSString*)identifier inCluster:(WMCluster*)cluster
{
    NSArray* fetchedPoints = [self.managedObjectContext fetchObjectsForEntityWithName:@"Point"
                                                                       sortByKeyOrNil:nil
                                                                        withPredicate:@"cluster = %@ AND identifier = %@",
                              cluster, identifier];
    if(!fetchedPoints || fetchedPoints.count != 1) {
        if(fetchedPoints.count > 1) {
//            DebugLog(@"found %d points with ID '%@'", fetchedPoints.count, identifier);
        }
        return nil;
    }
    
    return [fetchedPoints objectAtIndex:0];
}




#pragma mark - Core Data stack

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            // Handle criticall error
            DebugLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext
{
    if([NSThread isMainThread]) {
        if (__managedObjectContext != nil) {
            return __managedObjectContext;
        }
    } else {
        return [self managedObjectContextForThread:[NSThread currentThread]];
    }
    
    if(nil != self.persistentStoreCoordinator) {
        NSManagedObjectContext* moc = [[NSManagedObjectContext alloc]
                                       initWithConcurrencyType:NSMainQueueConcurrencyType];
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if(nil != coordinator) {
            [moc setPersistentStoreCoordinator:coordinator];
            [moc setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
        }
        __managedObjectContext = moc;
    }
    return __managedObjectContext;
}

-(NSManagedObjectContext*)managedObjectContextForThread:(NSThread*)thread
{
    static NSString *threadLock = @"Lock";
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removeManagedObjectContext:)
                                                 name:NSThreadWillExitNotification
                                               object:thread];
    
    @synchronized(threadLock) {
        for(NSDictionary *dic in managedObjectContexts) {
            NSManagedObjectContext *moc = [dic valueForKey:[NSString stringWithFormat:@"%@", thread]];
            if(nil != moc) {
                return moc;
            }
        }
        
        NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] init];
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if(nil != coordinator) {
            NSString *threadName = [NSString stringWithFormat:@"%@", thread];
            [moc setPersistentStoreCoordinator:coordinator];
            [moc setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
            NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:moc, threadName, nil];
            [managedObjectContexts addObject:dic];
        }
        return moc;
    }
}

- (void)removeManagedObjectContext:(NSNotification *) notification
{
    NSThread *thread = (NSThread*)[notification object];
    NSString *currentThread = [NSString stringWithFormat:@"%@", thread];
    @synchronized (@"Lock") {
        for(NSDictionary *dic in managedObjectContexts) {
            if(nil != [dic objectForKey:currentThread]) {
                [managedObjectContexts removeObject:dic];
                return;
            }
        }
    }
}


- (NSManagedObjectModel*)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }

    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"PointsData" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }

    NSString *fileName =  [DB_FOLDER_NAME stringByAppendingPathComponent:[DB_FILE_NAME stringByAppendingPathExtension:DB_FILE_TYPE]];
//    NSURL *storeURL = [[WMDBLayer applicationDocumentsDirectory] URLByAppendingPathComponent:fileName];
    NSURL *storeURL = [[WMDBLayer applicationLibraryDirectory] URLByAppendingPathComponent:fileName];
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                    initWithManagedObjectModel:[self managedObjectModel]];

    NSError *error = nil;
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                    configuration:nil URL:storeURL
                                                          options:nil error:&error])
    {
        // Handle critical error
        DebugLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return __persistentStoreCoordinator;
}

#pragma mark - Application's directories


- (BOOL)saveDB
{
    NSError *error = nil;
    NSManagedObjectContext *moc = self.managedObjectContext;
    if([moc hasChanges] && ![moc save:&error]) {
        DebugLog(@"WARNING: COULD NOT SAVE THE DATABASE!!");
        // Critical: this should never happen if the database is ok
//        abort();
        return NO;
    }
    return YES;
}

+ (NSURL*)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

+ (NSURL*)applicationLibraryDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask] lastObject];
}

+ (NSURL*)applicationCacheDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
}




@end




















