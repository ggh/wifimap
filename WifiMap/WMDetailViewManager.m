

#import "WMDetailViewManager.h"
#import "WMNearbyViewController.h"
#import "WMPointDetailsViewController.h"
#import "WMNewPointViewController.h"

@interface WMDetailViewManager ()
// Holds a reference to the split view controller's bar button item
// if the button should be shown (the device is in portrait).
// Will be nil otherwise.
@property (nonatomic, strong) UIBarButtonItem *navigationPaneButtonItem;
// Holds a reference to the popover that will be displayed
// when the navigation button is pressed.
@property (nonatomic, strong) UIPopoverController *navigationPopoverController;

@property (nonatomic, strong) UIImageView *shadow;
@end


@implementation WMDetailViewManager

- (id)init
{
    self = [super init];
    if(self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNearby) name:@"showNearby" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPointDetails) name:@"showPointDetails" object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNewPoint:) name:@"showNewPoint" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismiss) name:@"new_point_added" object:nil];
    }
    return self;
}

- (UIImageView*)shadow
{
    if(!_shadow) {
        _shadow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"inner_shadow.png"]];
        CGRect frame = _shadow.frame;
        frame.origin.x = self.navController.view.frame.size.width - frame.size.width + 4;
        frame.origin.y = 0;
        frame.size.height = [UIScreen mainScreen].bounds.size.height;
        _shadow.frame = frame;
        _shadow.tag = 0xAAA;
    }
    return _shadow;
    
//    CGRect frame = imgView.frame;
//    [self.navController.view addSubview:imgView];
    
}

- (void)showNearby
{
    if(![self.navController.view.subviews containsObject:self.shadow]) {
        [self.navController.view addSubview:self.shadow];
    }

//    DebugLog(@"action: %s %s", __FUNCTION__, sel_getName(self.navigationPaneButtonItem.action));

    WMNearbyViewController *nearby = [WMNearbyViewController sharedController];
    if(self.navController.viewControllers.firstObject == nearby) {
        if(self.navigationPopoverController.popoverVisible) {
            [self.navController popToRootViewControllerAnimated:YES];
            return;
        }
        else {
            [self.navController setViewControllers:[NSArray arrayWithObject:nearby]];
            if([self.splitViewController respondsToSelector:self.navigationPaneButtonItem.action]) {
                [self.splitViewController performSelector:self.navigationPaneButtonItem.action];
                return;
            }
        }
    }

    if(self.navController.viewControllers.count == 1) {
        // this is point details controller
        WMPointDetailsViewController *details = self.navController.viewControllers.firstObject;
        [self.navController setViewControllers:@[nearby, details]];
        [self.navController popToRootViewControllerAnimated:NO];
        return;
    }

    [self.navController setViewControllers:[NSArray arrayWithObject:nearby]];
    if([self.splitViewController respondsToSelector:self.navigationPaneButtonItem.action]) {
        [self.splitViewController performSelector:self.navigationPaneButtonItem.action];
    }
}

- (void)showPointDetails
{
    if(![self.navController.view.subviews containsObject:self.shadow]) {
        [self.navController.view addSubview:self.shadow];
    }

    WMPointDetailsViewController *pointDetails = [WMPointDetailsViewController sharedController];
    [self.navController setViewControllers:[NSArray arrayWithObject:pointDetails]];
    if([self.splitViewController respondsToSelector:self.navigationPaneButtonItem.action]) {
        [self.splitViewController performSelector:self.navigationPaneButtonItem.action];
    }
}

- (void)showNewPoint:(NSNotification*)notification
{
    if(![self.navController.view.subviews containsObject:self.shadow]) {
        [self.navController.view addSubview:self.shadow];
    }

    WMNewPointViewController *controller = [notification.userInfo objectForKey:@"controller"];
    NSString *name = [notification.userInfo objectForKey:@"point_name"];
    CLLocationCoordinate2D userCoord = CLLocationCoordinate2DMake([[notification.userInfo objectForKey:@"lat"] doubleValue],
                                                                  [[notification.userInfo objectForKey:@"lon"] doubleValue]);
    controller.name = name;
    controller.coordinate = userCoord;
    [self.navController setViewControllers:@[controller]];
    if([self.splitViewController respondsToSelector:self.navigationPaneButtonItem.action]) {
        [self.splitViewController performSelector:self.navigationPaneButtonItem.action];
    }
}

// -------------------------------------------------------------------------------
//	setDetailViewController:
//  Custom implementation of the setter for the detailViewController property.
// -------------------------------------------------------------------------------
- (void)setDetailViewController:(UIViewController<SubstitutableDetailViewController> *)detailViewController
{
    // Clear any bar button item from the detail view controller that is about to
    // no longer be displayed.
//    self.detailViewController.navigationPaneBarButtonItem = nil;
    
    _detailViewController = detailViewController;
    
    // Set the new detailViewController's navigationPaneBarButtonItem to the value of our
    // navigationPaneButtonItem.  If navigationPaneButtonItem is not nil, then the button 
    // will be displayed.
//    _detailViewController.navigationPaneBarButtonItem = self.navigationPaneButtonItem;
    
    // Update the split view controller's view controllers array.
    // This causes the new detail view controller to be displayed.
    UIViewController *navigationViewController = [self.splitViewController.viewControllers objectAtIndex:0];
    NSArray *viewControllers = [[NSArray alloc] initWithObjects:navigationViewController, _detailViewController, nil];
    self.splitViewController.viewControllers = viewControllers;
    
    // Dismiss the navigation popover if one was present.  This will
    // only occur if the device is in portrait.
    if (self.navigationPopoverController)
        [self.navigationPopoverController dismissPopoverAnimated:YES];
}

- (void)dismiss
{
    if([self.splitViewController respondsToSelector:self.navigationPaneButtonItem.action]) {
        [self.splitViewController performSelector:self.navigationPaneButtonItem.action];
    }
}

#pragma mark -
#pragma mark UISplitViewDelegate

// -------------------------------------------------------------------------------
//	splitViewController:shouldHideViewController:inOrientation:
// -------------------------------------------------------------------------------
- (BOOL)splitViewController:(UISplitViewController *)svc 
   shouldHideViewController:(UIViewController *)vc 
              inOrientation:(UIInterfaceOrientation)orientation
{
    return YES;
    return UIInterfaceOrientationIsPortrait(orientation);
    return NO;
}

// -------------------------------------------------------------------------------
//	splitViewController:willHideViewController:withBarButtonItem:forPopoverController:
// -------------------------------------------------------------------------------
- (void)splitViewController:(UISplitViewController *)svc 
     willHideViewController:(UIViewController *)aViewController 
          withBarButtonItem:(UIBarButtonItem *)barButtonItem 
       forPopoverController:(UIPopoverController *)pc
{
    // If the barButtonItem does not have a title (or image) adding it to a toolbar
    // will do nothing.
    barButtonItem.title = @"Navigation";
    
    self.navigationPaneButtonItem = barButtonItem;
    self.navigationPopoverController = pc;
    // Tell the detail view controller to show the navigation button.
//    self.detailViewController.navigationPaneBarButtonItem = barButtonItem;
}

// -------------------------------------------------------------------------------
//	splitViewController:willShowViewController:invalidatingBarButtonItem:
// -------------------------------------------------------------------------------
- (void)splitViewController:(UISplitViewController *)svc 
     willShowViewController:(UIViewController *)aViewController 
  invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    self.navigationPaneButtonItem = nil;
    self.navigationPopoverController = nil;
    
    // Tell the detail view controller to remove the navigation button.
//    self.detailViewController.navigationPaneBarButtonItem = nil;
}


@end
