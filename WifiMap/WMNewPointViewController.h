//
//  WMNewPointViewController.h
//  WifiMap
//
//  Created by Gor on 20/05/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface WMNewPointViewController : UIViewController

@property (strong, nonatomic) NSString *name;
@property CLLocationCoordinate2D coordinate;

@end
