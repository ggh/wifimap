//
//  WMNewPointViewController.m
//  WifiMap
//
//  Created by Gor on 20/05/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import "WMNewPointViewController.h"
#import "WMPointsHandler.h"

@interface WMNewPointViewController () <UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UISwitch *passwordSwitch;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet UIButton *sendButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation WMNewPointViewController

@synthesize name = _name;

- (id)init
{
    self = [super initWithNibName:@"WMNewPointViewController" bundle:nil];
    if(self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initElements];
    [self switchStateChanged];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.passwordField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    if(IS_IOS7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [super viewWillAppear:animated];

    if(IS_IOS7) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    }
    else {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}


- (void)setName:(NSString *)name
{
    _name = name;
    self.nameLabel.text = name;
}

- (NSString*)name
{
    return _name;
}


#pragma mark - private

- (void)initElements
{
    self.passwordField.placeholder = NSLocalizedString(@"password", @"");
    self.passwordLabel.text = NSLocalizedString(@"require_password", @"");
    [self.sendButton setTitle:NSLocalizedString(@"send", @"") forState:UIControlStateNormal];

    [self.sendButton addTarget:self action:@selector(sendNewPointRequest) forControlEvents:UIControlEventTouchUpInside];
    [self.passwordSwitch addTarget:self action:@selector(switchStateChanged) forControlEvents:UIControlEventValueChanged];
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self.passwordField action:@selector(resignFirstResponder)];
    UIPanGestureRecognizer *gesture2 = [[UIPanGestureRecognizer alloc] initWithTarget:self.passwordField action:@selector(resignFirstResponder)];
    [self.scrollView addGestureRecognizer:gesture];
    [self.scrollView addGestureRecognizer:gesture2];
    self.title = NSLocalizedString(@"new_hotspot", @"");
    self.nameLabel.text = self.name;
}

- (void)sendNewPointRequest
{
    [self.passwordField resignFirstResponder];

    [WMPointsHandler addNewPoint:self.coordinate name:self.name password:self.passwordField.text onFinish:^(BOOL success) {
        if(success) {
            NSString *message = NSLocalizedString(@"new_point_success", @"");
            NSString *buttonTitle = NSLocalizedString(@"dismiss", @"");
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:message
                                                               delegate:self
                                                      cancelButtonTitle:buttonTitle
                                                      otherButtonTitles:nil, nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                [alertView show];
            });
        }
        else {
            NSString *message = NSLocalizedString(@"server_error", @"");
            NSString *buttonTitle = NSLocalizedString(@"dismiss", @"");
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:message
                                                               delegate:nil
                                                      cancelButtonTitle:buttonTitle
                                                      otherButtonTitles:nil, nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                [alertView show];
            });
        }
    }];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(!IS_IPAD) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    }

    DebugLog(@"point added successfully!");

    [[NSNotificationCenter defaultCenter] postNotificationName:@"new_point_added" object:nil];
}

- (void)switchStateChanged
{
    if(!self.passwordSwitch.isOn) {
//        self.passwordField.enabled = YES;
        self.passwordField.hidden = NO;
    }
    else {
//        self.passwordField.enabled = NO;
        self.passwordField.hidden = YES;
        self.passwordField.text = nil;
    }
}

@end





















