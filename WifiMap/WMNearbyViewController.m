//
//  WMNearbyViewController.m
//  WifiMap
//
//  Created by Gor on 16/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import "WMNearbyViewController.h"
#import "WMDBLayer.h"
#import "WMPointDetailsViewController.h"
#import <MessageUI/MessageUI.h>

#import "WMDetailViewManager.h"

@interface WMNearbyViewController () <UITableViewDataSource, UITableViewDelegate, SubstitutableDetailViewController, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *glowImage;
@property (strong, nonatomic) IBOutlet UIButton *nearbyButton;
@property (strong, nonatomic) IBOutlet UILabel *rateLabel;
@property (strong, nonatomic) NSArray *nearbyPoints;
@property (strong, nonatomic) UIBarButtonItem *proButton;

- (IBAction)rateButtonAction:(UIButton *)sender;

@end

@implementation WMNearbyViewController

+ (instancetype)sharedController
{
    static WMNearbyViewController *sharedController = nil;
    if (nil != sharedController) {
        return sharedController;
    }

    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedController = [[WMNearbyViewController alloc] initWithNibName:@"WMNearbyViewController" bundle:nil];
        [sharedController view];
    });
    return sharedController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"wifi_nearby", @"");
    self.rateLabel.text = NSLocalizedString(@"rate_the_app", @"");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(IS_IOS7) {
        self.navigationController.navigationBarHidden = NO;
        CGFloat deltaShift = self.navigationController.navigationBar.bounds.size.height + [UIApplication sharedApplication].statusBarFrame.size.height;
        CGRect frame = self.glowImage.frame;
        frame.origin.y = deltaShift;
        self.glowImage.frame = frame;
        
        frame = self.nearbyButton.frame;
        frame.origin.y = deltaShift - 4;
        self.nearbyButton.frame = frame;
    }
    else {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
    
    [self.navigationItem setRightBarButtonItem:self.proButton];
    [self fixSubviewPositionsForOrientation:self.interfaceOrientation];

    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if(indexPath) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }

    // TMP. TODO: Ask if needs to be displayed
    self.nearbyButton.hidden = YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIBarButtonItem*)proButton
{
#if LITE_VERSION
    if(!_proButton) {
        //        _proButton = [[UIBarButtonItem alloc] initWithTitle:@"Pro" style:UIBarButtonItemStylePlain target:self action:@selector(proButtonAction)];
        _proButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Pro", @"") style:UIBarButtonItemStylePlain
                                                     target:self action:@selector(proButtonAction)];
        _proButton.tintColor = [UIColor redColor];
    }
#else
    _proButton = nil;
#endif
    return _proButton;
}


- (void)setUserCoord:(CLLocationCoordinate2D)userCoord
{
    _userCoord = userCoord;
    CLLocationCoordinate2D topLeft = CLLocationCoordinate2DMake(userCoord.latitude - 0.1,
                                                                userCoord.longitude - 0.1);
    CLLocationCoordinate2D bottomRight = CLLocationCoordinate2DMake(userCoord.latitude + 0.1,
                                                                userCoord.longitude + 0.1);
    NSArray *points = [WMDBLayer pointsForArea:topLeft bottomRight:bottomRight];

    self.nearbyPoints = nil;
    self.nearbyPoints = [self excludeOuter:points];
    [self.tableView reloadData];
}

- (void)setMaxVisibleDistance:(CLLocationDistance)maxVisibleDistance
{
    if(LITE_VERSION) {
        _maxVisibleDistance = maxVisibleDistance;
        [self.tableView reloadData];
    }
}

- (void)setMaxDistance:(CLLocationDistance)maxDistance
{
    _maxDistance = maxDistance;
    if(!LITE_VERSION) {
        _maxVisibleDistance = maxDistance;
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if(!IS_IOS7) {
        return;
    }
    
    [self fixSubviewPositionsForOrientation:toInterfaceOrientation];
}

#pragma mark - TableView Delegate & DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.nearbyPoints.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseId = @"comment_cell";
    UITableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:reuseId];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseId];
        [cell setBackgroundColor:[UIColor clearColor]];
    }

    static UIImage *arrowImage = nil;
    static UIImage *lockImage = nil;
    arrowImage = [UIImage imageNamed:@"arrow.png"];
    lockImage = [UIImage imageNamed:@"lock.png"];

    WMPoint *point = [self.nearbyPoints objectAtIndex:indexPath.row];
    if(LITE_VERSION && point.distance > self.maxVisibleDistance) {
        cell.accessoryView = [[UIImageView alloc] initWithImage:lockImage];
    }
    else {
        cell.accessoryView = [[UIImageView alloc] initWithImage:arrowImage];
    }
    cell.textLabel.text = point.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%dм, %@", (int)point.distance, point.address];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WMPoint *point = [self.nearbyPoints objectAtIndex:indexPath.row];
    if(LITE_VERSION && point.distance > self.maxVisibleDistance) {
        return;
    }
    WMPointDetailsViewController *controller = [WMPointDetailsViewController sharedController];
    controller.point = point;
    [self.navigationController pushViewController:controller animated:YES];
}



#pragma mark - Private

- (NSArray*)excludeOuter:(NSArray*)points
{
    NSMutableArray *innerPoints = [NSMutableArray array];
    CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:_userCoord.latitude longitude:_userCoord.longitude];

    for(WMPoint *point in points) {
        CLLocation *pointLocation = [[CLLocation alloc] initWithLatitude:point.lat.doubleValue longitude:point.lon.doubleValue];
        CLLocationDistance distance = [userLocation distanceFromLocation:pointLocation];
        if(distance  <= self.maxDistance) {
            [innerPoints addObject:point];
            point.distance = distance;
        }
    }

    NSArray * sortedPoints = [innerPoints sortedArrayUsingComparator:^NSComparisonResult(WMPoint *obj1, WMPoint *obj2) {
//        CLLocation *location1 = [[CLLocation alloc] initWithLatitude:obj1.lat.doubleValue longitude:obj1.lat.doubleValue];
//        CLLocation *location2 = [[CLLocation alloc] initWithLatitude:obj2.lat.doubleValue longitude:obj2.lat.doubleValue];
//        CLLocationDistance d1 = [location1 distanceFromLocation:userLocation];
//        CLLocationDistance d2 = [location2 distanceFromLocation:userLocation];

        double d1 = obj1.distance;
        double d2 = obj2.distance;
        NSComparisonResult result = NSOrderedSame;
        if(d1 > d2) {
            result = NSOrderedDescending;
        }
        else {
            result = NSOrderedAscending;
        }
        return result;
    }];
    
    return sortedPoints;
}

- (void)fixSubviewPositionsForOrientation:(UIInterfaceOrientation)orientation
{
    if(!IS_IOS7) {
        return;
    }
    CGRect frame1 = self.glowImage.frame;
    CGRect frame2 = self.nearbyButton.frame;
    CGFloat dY = 64;
    if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        dY = 52;
    }
    frame1.origin.y = dY;
    frame2.origin.y = dY - 4;
    self.glowImage.frame = frame1;
    self.nearbyButton.frame = frame2;
}

#if LITE_VERSION

- (void)proButtonAction
{
    NSURL *url = [NSURL URLWithString:kPRO_URL];
    [[UIApplication sharedApplication] openURL:url];
}

#endif



- (IBAction)rateButtonAction:(UIButton *)sender
{
    if(sender.tag == 0) {
        DebugLog(@"mail");

        if([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
            mailController.mailComposeDelegate = self;

            NSString *body = @"";
            body = [body stringByAppendingString:@"<br/>Feedback:"];

            [mailController setToRecipients:[NSArray arrayWithObjects:@"Sergey Zuev <zuev.sergey@gmail.com>", nil]];
            [mailController setSubject:@"Wifi Maps : Feedback"];
            [mailController setMessageBody:body isHTML:YES];
            
            [self presentModalViewController:mailController animated:YES];

            return;
        }
    }

    DebugLog(@"iTunes");
    NSURL *url =
#if LITE_VERSION
    [NSURL URLWithString: kLITE_URL];
#else
    [NSURL URLWithString: kPRO_URL];
#endif
    [GUtils sharedUtils].didRateGood = YES;
    [WMNearbyViewController sharedController].maxVisibleDistance = [[[NSBundle mainBundle]
                                                                     objectForInfoDictionaryKey:@"MaxVisibleDistanceExtended"] doubleValue];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissModalViewControllerAnimated:YES];
}

@end























