//
//  WMPointDetailsViewController.m
//  WifiMap
//
//  Created by Gor on 15/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import "WMPointDetailsViewController.h"
#import "WMPoint.h"
#import "WMNearbyViewController.h"
#import "WMTextView.h"
#import <MessageUI/MessageUI.h>
//#import "UITextField+Copying.h"

@interface WMPointDetailsViewController () <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *infoLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UILabel *rateLabel;
@property (strong, nonatomic) IBOutlet UIImageView *glowImage;
@property (strong, nonatomic) IBOutlet UIButton *nearbyButton;

@property (strong, nonatomic) UIBarButtonItem *proButton;

@property (strong, nonatomic) NSArray *comments;

- (IBAction)rateButtonClicked:(UIButton*)sender;

@end

@implementation WMPointDetailsViewController

+ (instancetype)sharedController
{
    static WMPointDetailsViewController *sharedController = nil;
    if (nil != sharedController) {
        return sharedController;
    }
    
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedController = [[WMPointDetailsViewController alloc] initWithNibName:@"WMPointDetailsViewController" bundle:nil];
        [sharedController view];
    });
    return sharedController;
}


- (id)init
{
    self = [super initWithNibName:@"WMPointDetailsViewController" bundle:nil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.nearbyButton addTarget:self action:@selector(showNearby) forControlEvents:UIControlEventTouchUpInside];
    self.rateLabel.text = NSLocalizedString(@"rate_the_app", @"");
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(IS_IOS7) {
        self.navigationController.navigationBarHidden = NO;
        CGRect frame = self.addressLabel.frame;
        CGFloat deltaShift = self.navigationController.navigationBar.bounds.size.height + [UIApplication sharedApplication].statusBarFrame.size.height;
        frame.origin.y = deltaShift + 4;
        self.addressLabel.frame = frame;

        frame = self.glowImage.frame;
        frame.origin.y = deltaShift;
        self.glowImage.frame = frame;

        frame = self.nearbyButton.frame;
        frame.origin.y = deltaShift - 4;
        self.nearbyButton.frame = frame;
    }
    else {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }

    [self.navigationItem setRightBarButtonItem:self.proButton];

    [self fixSubviewPositionsForOrientation:self.interfaceOrientation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIBarButtonItem*)proButton
{
#if LITE_VERSION
    if(!_proButton) {
//        _proButton = [[UIBarButtonItem alloc] initWithTitle:@"Pro" style:UIBarButtonItemStylePlain target:self action:@selector(proButtonAction)];
        _proButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Pro", @"") style:UIBarButtonItemStylePlain
                                                     target:self action:@selector(proButtonAction)];
        _proButton.tintColor = [UIColor redColor];
    }
#else
    _proButton = nil;
#endif
    return _proButton;
}

- (IBAction)rateButtonClicked:(UIButton*)sender
{
//    DebugLog(@"rate button click");
    if(sender.tag == 0) {
        DebugLog(@"mail");
        
        if([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
            mailController.mailComposeDelegate = self;
            
            NSString *body = @"";
            body = [body stringByAppendingString:@"<br/>Feedback:"];
            
            [mailController setToRecipients:[NSArray arrayWithObjects:@"Sergey Zuev <zuev.sergey@gmail.com>", nil]];
            [mailController setSubject:@"Wifi Maps : Feedback"];
            [mailController setMessageBody:body isHTML:YES];
            
            [self presentModalViewController:mailController animated:YES];
            
            return;
        }
    }
    
    DebugLog(@"iTunes");
    NSURL *url =
#if LITE_VERSION
    [NSURL URLWithString: kLITE_URL];
#else
    [NSURL URLWithString: kPRO_URL];
#endif

    [GUtils sharedUtils].didRateGood = YES;
    [WMNearbyViewController sharedController].maxVisibleDistance = [[[NSBundle mainBundle]
                                                                     objectForInfoDictionaryKey:@"MaxVisibleDistanceExtended"] doubleValue];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissModalViewControllerAnimated:YES];
}

- (void)setPoint:(WMPoint *)point
{
    NSArray *comments = [point.comment componentsSeparatedByString:@"\n\n"];
    if(!comments) {
        self.comments = [NSArray array];
    }
    else {
        self.comments = [NSArray arrayWithArray:comments];
    }
    [self.tableView reloadData];

    self.title = point.title;
    NSString *address = point.address.length > 0 ? point.address : @"";
    self.addressLabel.text = address;
    self.infoLabel.text = NSLocalizedString(@"point_details_help", @"");
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if(!IS_IOS7) {
        return;
    }

    [self fixSubviewPositionsForOrientation:toInterfaceOrientation];
}


#pragma mark - TableView Delegate & DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.comments.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *text = [self.comments objectAtIndex:indexPath.row];
    NSUInteger length = text.length;
    CGFloat height = length / 35 * 0.7f * 40 + 40;
    return height;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseId = [NSString stringWithFormat:@"comment_cell_%d", indexPath.row];
    UITableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:reuseId];
    NSString *text = [self.comments objectAtIndex:indexPath.row];

    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseId];
        [cell setBackgroundColor:[UIColor clearColor]];
        UITapGestureRecognizer *doubleTapRecogniser = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(copyCellInfo:)];
        doubleTapRecogniser.numberOfTapsRequired = 2;
//        [doubleTapRecogniser addTarget:self action:@selector(copyCellInfo:)];
        [cell addGestureRecognizer:doubleTapRecogniser];
    }

    for(UIView *v in cell.contentView.subviews) {
        if([v isKindOfClass:[UITextView class]]) {
            [v removeFromSuperview];
        }
    }

    cell.textLabel.numberOfLines = 0;
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
//    if(text.length > 30) {
//        cell.textLabel.numberOfLines = 2;
//    }
//    else {
//        cell.textLabel.numberOfLines = 1;
//    }

    cell.textLabel.text = text;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *text = [self.comments objectAtIndex:indexPath.row];
    NSUInteger length = text.length;
    CGFloat height = length % 35 * 0.5f + 40;
    return height;
    if(length > 30) {
        return 60;
    }
    return 40;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *text = nil;
    for(UIView *v in cell.contentView.subviews) {
        if([v isKindOfClass:[UITextView class]]) {
            text = [(UITextView*)v text];
            [v removeFromSuperview];
            break;
        }
    }
    if(text) {
        cell.textLabel.text = text;
    }
    [tableView reloadData];
}


#pragma mark - TextField delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return NO;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if(action == @selector(copy:)) {
        return YES;
    }
    return NO;
}

- (id)targetForAction:(SEL)action withSender:(id)sender
{
    if(action == @selector(copy:)) {
        return self;
    }
    return nil;
}

#pragma mark - private

- (void)copyCellInfo:(UITapGestureRecognizer*)sender
{
    UITableViewCell *cell = (UITableViewCell*)(sender.view);
    [cell setSelected:NO animated:YES];
    NSString *content = cell.textLabel.text;
    [[UIPasteboard generalPasteboard] setString:content];

//    UITextView *tField = [[UITextView alloc] initWithFrame:cell.contentView.frame];
    WMTextView *tField = [[WMTextView alloc] initWithFrame:cell.contentView.frame];
    tField.font = cell.textLabel.font;
    tField.text = content;
    if(IS_IOS7) {
        tField.selectable = YES;
    }
    tField.editable = NO;
    tField.delegate = self;
    [cell.contentView addSubview:tField];
    cell.textLabel.text = @"";
}

- (void)fixSubviewPositionsForOrientation:(UIInterfaceOrientation)orientation
{
    if(!IS_IOS7) {
        return;
    }
    CGRect frame1 = self.glowImage.frame;
    CGRect frame2 = self.nearbyButton.frame;
    CGFloat dY = 64;
    if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        dY = 52;
    }
    frame1.origin.y = dY;
    frame2.origin.y = dY - 4;
    self.glowImage.frame = frame1;
    self.nearbyButton.frame = frame2;
}

- (void)showNearby
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(nearbyButtonTapped)]) {
        [self.delegate nearbyButtonTapped];
    }
}

#if LITE_VERSION

- (void)proButtonAction
{
    NSURL *url = [NSURL URLWithString:kPRO_URL];
    [[UIApplication sharedApplication] openURL:url];
}

#endif

@end





















