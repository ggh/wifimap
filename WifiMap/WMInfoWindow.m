//
//  WMInfoWindow.m
//  WifiMap
//
//  Created by Gor on 14/03/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#import "WMInfoWindow.h"
#import "WMDBLayer.h"
#import <QuartzCore/QuartzCore.h>

@interface WMInfoWindow()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *detailLabel1;
@property (nonatomic, strong) UILabel *detailLabel2;

@property (nonatomic, strong) UIImage *topImage;
@property (nonatomic, strong) UIImage *topImageDisabled;
@property (nonatomic, strong) UIImageView *topImageView;
- (void)initSubviews;

@end

@implementation WMInfoWindow

- (id)initWithFrame:(CGRect)frame
{
    // hack to refill the area lost by shadow in image
    frame.size.height *= 1.2f;

    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews];
    }
    return self;
}

+ (instancetype)infoWindowForPoint:(WMPoint *)point
{
    return [self infoWindowForPoint:point disabled:NO];
}

+ (instancetype)infoWindowForPoint:(WMPoint *)point disabled:(BOOL)disabled
{
    static WMInfoWindow __strong *sharedWindow = nil;
    if(sharedWindow == nil) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            CGRect frame = CGRectMake(0, 0, 220, 85);
            if(IS_IPAD) {
                // TODO: set other size here if needed
                frame = CGRectMake(0, 0, 220, 85);
            }
            sharedWindow = [[WMInfoWindow alloc] initWithFrame:frame];
        });
    }

    if(!point) {
        return nil;
    }

    sharedWindow.topImageView.image = disabled ? sharedWindow.topImageDisabled : sharedWindow.topImage;

    sharedWindow.titleLabel.text = disabled ? NSLocalizedString(@"pro_only", @"") : point.title;
    NSArray *comments = [point.comment componentsSeparatedByString:@"\n\n"];
    if(comments && comments.count > 0) {
        sharedWindow.detailLabel1.text = comments.firstObject;
    }
    sharedWindow.detailLabel2.text = point.address;

    return sharedWindow;
}


#pragma mark - private

- (void)initSubviews
{
//    UIImage *bgImage = [[UIImage imageNamed:@"ann_view_bg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 24, 24)];
    UIImage *bgImage = [[UIImage imageNamed:@"ann_view_bg_.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(23, 19, 24, 20)
                                                                           resizingMode:UIImageResizingModeStretch];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:self.frame];
    imgView.image = bgImage;

    CGRect topImageViewFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height / 2.75f);
    // hack because of shadows..
    topImageViewFrame.origin.x += self.frame.size.width * 0.063f;
    topImageViewFrame.size.width = self.frame.size.width * 0.875f;
    topImageViewFrame.origin.y += self.frame.size.height * 0.07f;
    topImageViewFrame.size.height = self.frame.size.height / 3.8f;

    self.topImage = [[UIImage imageNamed:@"ann_view_top.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(8, 8, 8, 8)];
    self.topImageDisabled = [[UIImage imageNamed:@"ann_view_top_dis.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(8, 8, 8, 8)];
    self.topImageView = [[UIImageView alloc] initWithFrame:topImageViewFrame];
    self.topImageView.image = self.topImage;

    CGRect labelFrame = CGRectMake(2, 2, topImageViewFrame.size.width - 4, topImageViewFrame.size.height - 4);
    // hack because of shadows..
    labelFrame.origin.x += self.frame.size.width * 0.063f;
    labelFrame.size.width = self.frame.size.width * 0.875f;

    self.titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
    self.titleLabel.center = self.topImageView.center;
    CGRect f = self.titleLabel.frame;
    f.origin.y -= 2;
    self.titleLabel.frame = f;

    labelFrame.origin.y = topImageViewFrame.size.height + 8;
    // hack because of shadows..
    labelFrame.origin.y += 5;

    labelFrame.size.height -= 10;
    self.detailLabel1 = [[UILabel alloc] initWithFrame:labelFrame];
    labelFrame.origin.y += labelFrame.size.height + 4;

    // hack because of shadows..
    labelFrame.origin.y += 5;

    self.detailLabel2 = [[UILabel alloc] initWithFrame:labelFrame];

    self.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    self.detailLabel1.font = [UIFont systemFontOfSize:12];
    self.detailLabel2.font = [UIFont systemFontOfSize:12];
    self.titleLabel.backgroundColor = self.detailLabel1.backgroundColor = self.detailLabel2.backgroundColor = [UIColor clearColor];

    self.titleLabel.textAlignment = self.detailLabel1.textAlignment = self.detailLabel2.textAlignment = NSTextAlignmentCenter;

    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.titleLabel.layer.shadowOffset = CGSizeMake(0, -1);
    self.titleLabel.layer.shadowRadius = 0.05f;
    self.titleLabel.layer.shadowOpacity = 1;

    self.detailLabel1.textColor = [UIColor blackColor];
    self.detailLabel1.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.detailLabel1.layer.shadowOffset = CGSizeMake(0, 1);
    self.detailLabel1.layer.shadowRadius = 0.05f;
    self.detailLabel1.layer.shadowOpacity = 1;

    self.detailLabel2.textColor = [UIColor grayColor];

    [self addSubview:imgView];
    [self addSubview:self.topImageView];
    [self addSubview:self.titleLabel];
    [self addSubview:self.detailLabel1];
    [self addSubview:self.detailLabel2];
    self.layer.cornerRadius = 5;

    self.layer.masksToBounds = YES;
}


@end

















