//
//  HeadersLite.h
//  WifiMap
//
//  Created by Gor on 22/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#ifndef WifiMap_HeadersLite_h
#define WifiMap_HeadersLite_h

    #ifdef LITE_VERSION
    #undef LITE_VERSION
    #endif

    #define LITE_VERSION true

    // Map sdk definition
    #define MAP_H       <GoogleMaps/GoogleMaps.h>
    // Map view definition
    #define MAP_VIEW    GMSMapView

#endif
