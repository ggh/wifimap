//
//  Headers.h
//  WifiMap
//
//  Created by Gor on 22/02/2014.
//  Copyright (c) 2014 G. All rights reserved.
//

#ifndef WifiMap_Headers_h
#define WifiMap_Headers_h

    #ifdef LITE_VERSION
    #undef LITE_VERSION
    #endif

    #define LITE_VERSION false

    // Map sdk definition
    #define MAP_H       "MapBox.h"
    // Map view definition
    #define MAP_VIEW    RMMapView

#endif
